#ifndef FTT_SE_WRAPPER_H
#define	FTT_SE_WRAPPER_H


typedef struct stream stream;
typedef struct FTTSE_MESSAGE FTTSE_MESSAGE;

int ftt_se_wrapper_new_stream(stream ** stream_ptr, unsigned short msg_id, unsigned short sync_type, unsigned char fifo_size);

int ftt_se_wrapper_start(char *device_name);

int ftt_se_wrapper_stop();

int ftt_se_wrapper_close_stream(stream * stream_ptr);

int ftt_se_wrapper_retag_stream(stream * stream_ptr);

int ftt_se_wrapper_rebind_stream(stream * stream_ptr);

int ftt_se_wrapper_initialize_producer(stream * stream_ptr);

int ftt_se_wrapper_initialize_consumer(stream * stream_ptr);

int ftt_se_wrapper_set_stream_parameters(stream * stream_ptr, unsigned int msg_size, unsigned short maximum_latency_in_ecs);

int ftt_se_wrapper_send(stream * stream_ptr, void * msg_ptr, unsigned int msg_size, unsigned short blocking_flag);

int ftt_se_wrapper_non_blocking_synchronous_send(stream * stream_ptr, void * msg_ptr, unsigned int msg_size);

int ftt_se_wrapper_receive(stream * stream_ptr, void * msg_ptr, unsigned int msg_size);


#endif	/* FTT_SE_WRAPPER_H */
