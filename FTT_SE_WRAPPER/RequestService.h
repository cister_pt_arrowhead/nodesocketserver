/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   RequestService.h
 * Author: root
 *
 * Created on July 7, 2016, 4:49 PM
 */

#ifndef REQUESTSERVICE_H
#define REQUESTSERVICE_H



#ifdef __cplusplus
extern "C" {
#endif
    /**
     *  Orchestrates Service.
     * @param pathFile FilePath.
     * @return 
     */
    void * requestService(void * pathFile);


#ifdef __cplusplus
}
#endif

#endif /* REQUESTSERVICE_H */

