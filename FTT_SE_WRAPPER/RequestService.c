/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

#include <stdio.h>
#include <string.h>

#include "SocketClient.h"
#include "Properties.h"
#include "FileReader.h"

void * requestService(void * pathFile) {
    char c;
    int ret = 1;
    char * buffer;
    char bufferTemp[MAX_BUFFER_SIZE];
    long temp;
    printf("\n Requesting service ... \n");

    // read file ... and put file content to buffer and set size
    ret = openFile1(&buffer, &temp, pathFile);
    if (ret < 0) {
        return (void*) ret;
    }

    strcpy(bufferTemp, ID_REQUEST_SERVICE);
    int size = strlen(ID_REQUEST_SERVICE);
    strcat(bufferTemp, "\n");

    temp += size + 1;
    strcat(bufferTemp, buffer);

    ret = socket_orchestrateService(bufferTemp, temp);
    if (ret < 0) {
        return (void*) ret;
    }

    return (void*) ret;
}