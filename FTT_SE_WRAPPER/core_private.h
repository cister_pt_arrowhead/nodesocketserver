#ifndef CORE_PRIVATE_H
#define CORE_PRIVATE_H

#define CAN_START_STREAM 2233
#define CANNOT_START_STREAM 1122

#include <semaphore.h>

//static sem_t createStream;

extern char * arrowhead_system;

typedef struct application_stream application_stream;

typedef struct application application;

unsigned int get_application_stream_sequence_number(application_stream* application_stream_ptr);

unsigned short get_application_stream_msg_id(application_stream* application_stream_ptr);

int get_application_stream_monitoring_sockfd(application_stream* application_stream_ptr);

unsigned char get_application_stream_monitoring_sockfd_init(application_stream* application_stream_ptr);

void set_application_stream_monitoring_sockfd(application_stream * application_stream_ptr, int monitoring_sockfd);

void set_application_stream_monitoring_sockfd_init(application_stream * application_stream_ptr, unsigned char monitoring_sockfd_init);

int create_producer_stream(application_stream * application_stream_ptr, int stream_type);

int create_consumer_stream(application_stream * application_stream_ptr, int stream_type);

int add_new_application_stream(application * application_ptr, unsigned short msg_id, unsigned int msg_size, unsigned short period, unsigned short mtu, unsigned char qos_bandwidth, unsigned char qos_delay, unsigned char qos_response_time, int isProducer, unsigned short stream_type);

int application_stream_send(application_stream * application_stream_ptr, void* msg_ptr, unsigned int msg_size);

int get_application_type(application* application_ptr);

struct queue_root * get_application_stream_monitor_queue(application_stream * application_stream_ptr);

void set_application_stream_monitor_queue(application_stream * application_stream_ptr, struct queue_root * monitor_queue);

int add_log_monitor(application_stream * application_stream_ptr, char * content);

int get_log_monitor(application_stream * application_stream_ptr, char * content);

pthread_mutex_t get_application_stream_lock(application_stream* application_stream_ptr);

int lock_access(application_stream* application_stream_ptr);

int unlock_access(application_stream* application_stream_ptr);

void set_application_stream_type(application_stream * application_stream_ptr, int type);

int get_application_stream_type(application_stream * application_stream_ptr);


#endif

