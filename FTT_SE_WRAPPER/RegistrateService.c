
#include <stdio.h>
#include <string.h>

#include "Properties.h"
#include "FileReader.h"
#include "SocketClient.h"

/**
 * Registrates Service
 * @param pathFile
 * @return
 */
void * registrateService(void * pathFile1) {
    char * pathFile = (char*) pathFile1;

    printf("\n registrate service\n");
    //char pathFile[1024];
    char buffer[MAX_BUFFER_SIZE];
    char bufferTemp[MAX_BUFFER_SIZE];

    int temp = MAX_BUFFER_SIZE;
    int ret = openFile(buffer, &temp, pathFile);
    if (ret < 0) {
        return (void*) ret;
    }

    strcpy(bufferTemp, ID_REGISTERING_SERVICE);
    int size = strlen(ID_REGISTERING_SERVICE);
    strcat(bufferTemp, "\n");

    // size + 1, 1 from the char \n
    temp += size + 1;
    strcat(bufferTemp, buffer);

    // sendSocket
    socket_registrateService(bufferTemp, temp);

}
