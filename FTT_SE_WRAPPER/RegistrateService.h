/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/*
 * File:   RegistrateService.h
 * Author: root
 *
 * Created on July 7, 2016, 3:22 PM
 */

#ifndef REGISTRATESERVICE_H
#define REGISTRATESERVICE_H

#ifdef __cplusplus
extern "C" {
#endif


    /**
     * Registrates Service
     * @param pathFile
     * @return
     */
    void * registrateService(void * pathFile1);



#ifdef __cplusplus
}
#endif

#endif /* REGISTRATESERVICE_H */

