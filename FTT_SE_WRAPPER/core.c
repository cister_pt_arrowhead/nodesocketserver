
#include <string.h>
#include <pthread.h>

#include "core_private.h"
#include "core_public.h"

#include "SocketServer.h"
#include "ftt_se_wrapper.h"
#include "SocketClient.h"
#include "RegistrateService.h"
#include "real_time_utils.h"
#include "RequestService.h"
#include "DeleteService.h"
#include "pkt_queue.h"
#include "MonitorLogSender.h"
#include "FileReader.h"
#include "ErrorCommunicater.h"

#define SYSTEM_PATH_TO_FILE "System.txt"
#define ATRIBUI_THREAD_SLEEP_TIME 10000



char * arrowhead_system;

struct application_stream {
    stream * stream_ptr;

    unsigned int sequence_number;
    unsigned int last_sequence_number;

    unsigned int stream_size;
    unsigned short period;
    unsigned short msg_id;
    unsigned short mtu;

    unsigned char qos_bandwidth;
    unsigned char qos_delay;
    unsigned char qos_response_time;

    int monitoring_sockfd;
    unsigned char monitoring_sockfd_init;

    int type; // producer 1 consumer 2 
    int streamType; // 0 SYNCH 1 ASYNCH 2 ASYNCH 3 ASYNCH
    unsigned int node_id;
    void* next;

    pthread_t sender_thread_id;

    pthread_t monitor_thread_id;
    struct queue_root * monitor_queue;
    pthread_mutex_t lock;


};

struct application {
    unsigned int id;
    unsigned char type; // producer 1 consumer 2
    unsigned int socketServerPort;
    application_stream * head;
    unsigned int number_of_streams;
    pthread_t socket_server_thread_id;
};

struct STREAM_THREAD {
    application * application_ptr;
    void* function_name;
};

int read_system(char * path_to_file) {
    long max_size = 0;
    char * buffer;
    int ret = openFile1(&buffer, &max_size, path_to_file);
    remove_all_chars(&buffer, '\n');
    memmove(&arrowhead_system, &buffer, max_size);
    return ret;
}

unsigned int get_application_stream_sequence_number(application_stream* application_stream_ptr) {
    return application_stream_ptr->sequence_number;
}

pthread_t get_thread(application_stream* application_stream_ptr) {
    return application_stream_ptr->sender_thread_id;
}

void set_thread(application_stream * application_stream_ptr, pthread_t sender_thread_id) {
    application_stream_ptr->sender_thread_id = sender_thread_id;
}

unsigned int get_msg_size(application_stream* application_stream_ptr) {
    return application_stream_ptr->stream_size;
}

void set_application_stream_stream_size(application_stream * application_stream_ptr, unsigned int stream_size) {
    application_stream_ptr->stream_size = stream_size;
}

void* get_next_stream(application_stream * application_stream_ptr) {
    return application_stream_ptr->next;
}

void set_application_stream_next(application_stream * application_stream_ptr, void* next) {
    application_stream_ptr->next = next;
}

struct queue_root * get_application_stream_monitor_queue(application_stream * application_stream_ptr) {
    return application_stream_ptr->monitor_queue;
}

void set_application_stream_monitor_queue(application_stream * application_stream_ptr, struct queue_root * monitor_queue) {
    application_stream_ptr->monitor_queue = monitor_queue;
}

unsigned int get_application_socketServerPort(application * application_ptr) {
    return application_ptr->socketServerPort;
}

application_stream* get_top_stream(application* application) {
    return application->head;
}

void set_top_stream(application * application, application_stream * head) {
    application->head = head;
}

unsigned int get_number_of_streams(application* application) {
    return application->number_of_streams;
}

void set_number_of_streams(application * application, unsigned int number_of_streams) {
    application->number_of_streams = number_of_streams;
}

unsigned short get_application_stream_msg_id(application_stream* application_stream_ptr) {
    return application_stream_ptr->msg_id;
}

int get_application_stream_monitoring_sockfd(application_stream* application_stream_ptr) {
    return application_stream_ptr->monitoring_sockfd;
}

int lock_access(application_stream* application_stream_ptr) {
    pthread_mutex_lock(&(application_stream_ptr->lock));
    return 0;
}

int unlock_access(application_stream* application_stream_ptr) {
    pthread_mutex_unlock(&(application_stream_ptr->lock));
    return 0;
}

unsigned char get_application_stream_monitoring_sockfd_init(application_stream* application_stream_ptr) {
    return application_stream_ptr->monitoring_sockfd_init;
}

void set_application_stream_monitoring_sockfd(application_stream * application_stream_ptr, int monitoring_sockfd) {
    application_stream_ptr->monitoring_sockfd = monitoring_sockfd;
}

void set_application_stream_monitoring_sockfd_init(application_stream * application_stream_ptr, unsigned char monitoring_sockfd_init) {
    application_stream_ptr->monitoring_sockfd_init = monitoring_sockfd_init;
}

void set_application_stream_type(application_stream * application_stream_ptr, int type) {
    application_stream_ptr->type = type;
}

int get_application_stream_type(application_stream * application_stream_ptr) {
    return application_stream_ptr->type;
}

int get_application_type(application* application_ptr) {
    return application_ptr->type;
}

pthread_mutex_t stream_creation_mutex;

unsigned char stream_creation_state;

void set_creation_mutex_state(unsigned char new_state) {
    pthread_mutex_lock(&stream_creation_mutex);
    stream_creation_state = new_state;
    pthread_mutex_unlock(&stream_creation_mutex);
}

unsigned char get_creation_mutex_state() {
    pthread_mutex_lock(&stream_creation_mutex);
    unsigned char temp = stream_creation_state;
    pthread_mutex_unlock(&stream_creation_mutex);
    return temp;
}

int new_application(application ** application_ptr, int type, unsigned int socketServerPort) {
    application * temp_app_ptr;
    if ((temp_app_ptr = calloc(1, sizeof (application))) == 0) {
        return -1;
    }

    temp_app_ptr->id = 50;
    temp_app_ptr->head = 0;
    temp_app_ptr->type = type;
    temp_app_ptr->number_of_streams = 0;
    temp_app_ptr->socket_server_thread_id = 0;
    temp_app_ptr->socketServerPort = socketServerPort;

    *application_ptr = temp_app_ptr;
    // READ FILE
    int ret = read_system(SYSTEM_PATH_TO_FILE);

    if (ret < 0) {
        return -1;
    }

    return init_everything(temp_app_ptr);
}

/* 
 * FUNCAO QUE CRIA UMA THREAD RECEIVE PARA CADA STREAM
 */
void * atribui_threads(void * args) {

    struct STREAM_THREAD * arg = (struct STREAM_THREAD*) args;
    application * application_ptr = arg->application_ptr;
    void* function_name = arg->function_name;

    if (application_ptr == 0) {
        return;
    }

    while (1) {
        usleep(ATRIBUI_THREAD_SLEEP_TIME);

        application_stream * temp = get_top_stream(application_ptr);
        int last = get_number_of_streams(application_ptr);
        if (temp == 0) continue;
        do {
            pthread_t id;
            if (get_thread(temp) == 0) {
                if (pthread_create(&id, NULL, function_name, (void*) temp) < 0) {
                    printf("\nCannot Create Thread\n");
                    continue;
                }
                set_thread(temp, id);
            }
            last--;
            temp = get_next_stream(temp);
        } while (last > 0);
    }
}

int plugin_start(application ** application_ptr, int type, unsigned int socketServerPort, char * interface, void * functionName) {
    int ret = 0;
    ret = ftt_se_wrapper_start(interface);

    if (ret < 0) {
        return ret;
    }
    new_application(application_ptr, type, socketServerPort);
    pthread_t id;
    struct STREAM_THREAD temp;
    temp.application_ptr = *application_ptr;
    temp.function_name = functionName;
    pthread_create(&id, NULL, atribui_threads, (void*) &temp);
}

int init_everything(application * application_ptr) {
    if (pthread_create(&(application_ptr->socket_server_thread_id), NULL, runServer, (void*) application_ptr) < 0) {
        perror("could not create thread");
        return -1;
    }
    pthread_detach(application_ptr->socket_server_thread_id);

    return 1;
}

int initiate_monitoring(application_stream * new_application_stream) {
    // Initiate a queue
    init_queue(&new_application_stream->monitor_queue);

    monitor_thread_data * arg = calloc(1, sizeof (monitor_thread_data));
    arg->queue = &(new_application_stream->monitor_queue);
    arg->sockfd = &(new_application_stream->monitoring_sockfd);
    arg->sockfd_init = &(new_application_stream->monitoring_sockfd_init);
    arg->type = new_application_stream->type;

    pthread_t id;
    if (pthread_create(&id, NULL, monitor_socket_sender_thread, (void*) arg) < 0) {
        printf("\nThe Monitoring Thread Cannot Be Created!\n");
        return -1;
    }
    pthread_detach(id);
    new_application_stream->monitor_thread_id = id;

    return 1;
}

application_stream * allocate_application_stream(int type) {
    application_stream * new_application_stream;
    if ((new_application_stream = calloc(1, sizeof (application_stream))) == 0) {
        return 0;
    }
    new_application_stream->stream_ptr = 0;

    new_application_stream->sequence_number = 0;
    new_application_stream->last_sequence_number = -1;


    new_application_stream->stream_size = 0;
    new_application_stream->period = 0;
    new_application_stream->msg_id = 0;
    new_application_stream->mtu = 0;

    new_application_stream->qos_bandwidth = 0;
    new_application_stream->qos_delay = 0;
    new_application_stream->qos_response_time = 0;

    new_application_stream->monitoring_sockfd = 0;
    new_application_stream->monitoring_sockfd_init = 0;

    new_application_stream->node_id = 0;
    new_application_stream->next = 0;

    if (type == 1) {
        //Producer
        new_application_stream->type = 1;
    } else {
        //Consumer 
        new_application_stream->type = 2;
    }

    new_application_stream->sender_thread_id = 0;
    new_application_stream->monitor_thread_id = 0;
    new_application_stream->monitor_queue = 0;
    pthread_mutex_init(&new_application_stream->lock, NULL);

    initiate_monitoring(new_application_stream);

    return new_application_stream;
}

int create_producer_stream(application_stream * application_stream_ptr, int stream_type) {
    printf("\n\nCreating Producer Streamm....");
    int ret = 0;

    // atomic
    printf("create_producer_stream  id:%d\n", application_stream_ptr->msg_id);

    ret = ftt_se_wrapper_new_stream(&application_stream_ptr->stream_ptr, application_stream_ptr->msg_id, (unsigned short) stream_type, 1 /*fifo size*/);
    if (ret < 0) {
        if (ret == -3)
            sendError(application_stream_ptr->msg_id, "Stream Mode is Unrecognized - selected value is different from -3.");
        printf("\nError on ftt_se_wrapper_new_stream %d", ret);
        return ret;
    }

    printf("\nPRODUCER");
    // change size with seq number
    ret = ftt_se_wrapper_set_stream_parameters(application_stream_ptr->stream_ptr, application_stream_ptr->stream_size + (sizeof (unsigned int)*2), application_stream_ptr->period);
    if (ret < 0) {
        sendError(application_stream_ptr->msg_id, "Error Defining Stream Parameters");
        return ret;
    }


    ret = ftt_se_wrapper_initialize_producer(application_stream_ptr->stream_ptr);
    if (ret < 0) {
        if (ret == -3) sendError(application_stream_ptr->msg_id, "The Stream Is Already Initialized");
        if (ret == -5) sendError(application_stream_ptr->msg_id, "Error binding the stream");
    }
    if (ret < 0) {
        printf("Error In function initialize_producer");

        return ret;
    }


    return 1;
}

void waitForResponse_consumer() {
    printf("\n Waiting For The Orchestrator Response...");
    //sem_wait(&createStream);
    printf("\n Creating Stream \n");
}

int create_consumer_stream(application_stream * application_stream_ptr, int stream_type) {
    // Clean Screeen
    //printf("\033c");
    printf("\n\nCreating Consumer Streamm....");
    int ret = 0;
    //waitForResponse_consumer();


    // atomic
    ret = ftt_se_wrapper_new_stream(&application_stream_ptr->stream_ptr, application_stream_ptr->msg_id, (unsigned short) stream_type, 1 /*fifo size*/);
    if (ret < 0) {
        printf("\nError on ftt_se_wrapper_new_stream");
        return ret;
    }
    printf("\nCONSUMER");
    ret = ftt_se_wrapper_initialize_consumer(application_stream_ptr->stream_ptr);
    if (ret < 0) {
        if (ret == -3) sendError(application_stream_ptr->msg_id, "The Stream Is Already Initialized");
        if (ret == -5) sendError(application_stream_ptr->msg_id, "Error binding the stream");
        printf("\n Error in function initialize_consumer");
        return ret;
    }

    return 1;
}

int add_new_application_stream(application * application_ptr, unsigned short msg_id, unsigned int msg_size, unsigned short period, unsigned short mtu, unsigned char qos_bandwidth, unsigned char qos_delay, unsigned char qos_response_time, int isProducer, unsigned short stream_type) {
    application_stream * new_application_stream = allocate_application_stream(isProducer);
    if (new_application_stream == 0) {
        return-1;
    }

    application_stream * temp = application_ptr->head;
    if (application_ptr->head == 0) {

        application_ptr->head = new_application_stream;
        application_ptr->number_of_streams++;
        new_application_stream->node_id = application_ptr->number_of_streams;


        new_application_stream->stream_size = msg_size;
        new_application_stream->period = period;
        new_application_stream->msg_id = msg_id;
        new_application_stream->mtu = mtu;

        new_application_stream->qos_bandwidth = qos_bandwidth;
        new_application_stream->qos_delay = qos_delay;
        new_application_stream->qos_response_time = qos_response_time;
        new_application_stream->streamType = stream_type;

        if (isProducer == 1) {
            new_application_stream->type = 1;
        } else {
            new_application_stream->type = 2;
        }
        /*
                if (application_ptr->type == 1) {
                    if (create_producer_stream(new_application_stream) < 0) {
                        return -2;
                    }
                }
         */

        if (application_ptr->type == 2) {
            printf("\n Consumer Type \n");
            if (create_consumer_stream(new_application_stream, stream_type) < 0) {
                return -3;
            }
        }


        return (int) new_application_stream->node_id;
    }

    while (temp->next != 0) temp = temp->next;
    temp->next = new_application_stream;
    application_ptr->number_of_streams++;
    new_application_stream->node_id = application_ptr->number_of_streams;

    new_application_stream->stream_size = msg_size;
    new_application_stream->period = period;
    new_application_stream->msg_id = msg_id;
    new_application_stream->mtu = mtu;

    new_application_stream->qos_bandwidth = qos_bandwidth;
    new_application_stream->qos_delay = qos_delay;
    new_application_stream->qos_response_time = qos_response_time;
    new_application_stream->streamType = stream_type;

    if (application_ptr->type == 1) {
        if (create_producer_stream(new_application_stream, stream_type) < 0) {
            return -4;
        }
    }

    if (application_ptr->type == 2) {
        if (create_consumer_stream(new_application_stream, stream_type) < 0) {

            return -5;
        }
    }

    return (int) new_application_stream->node_id;
}

//registar 

int application_registration(char * pathToFile) {
    // ler ficheiro de
    pthread_t id;
    if (pthread_create(&id, NULL, registrateService, (void*) pathToFile) < 0) return -1;
    pthread_detach(id);
    return 1;
}

int application_orchestration(char * pathToFile) {
    pthread_t id;
    //sem_init(&createStream, 0, 0);
    if (pthread_create(&id, NULL, requestService, (void*) pathToFile) < 0) return -1;
    pthread_detach(id);
    return 1;
}

int application_deletion(char * pathToFile) {
    // ler ficheiro de
    deleteService(pathToFile);
    return 1;
}

int ssend(application_stream * application_stream_ptr, void* msg_ptr, unsigned int msg_size) {
    void * temp;

    if (application_stream_ptr->stream_ptr == 0) {
        if (create_producer_stream(application_stream_ptr, application_stream_ptr->streamType) < 0) {
            sendError(application_stream_ptr->msg_id, "Cannot Create Producer Stream");
            return -2;
        }
    }

    temp = calloc(1, application_stream_ptr->stream_size + (sizeof (unsigned int)*2));
    memmove(temp, &(application_stream_ptr->sequence_number), sizeof (unsigned int));
    memmove(temp + sizeof (unsigned int), &msg_size, sizeof (unsigned int));
    memmove(temp + sizeof (unsigned int)*2, msg_ptr, msg_size);
    int temp1 = *(int*) (temp + sizeof (unsigned int)*2);


    int ret = 0;
    struct timespec before, after, difference;

    clock_gettime(CLOCK_MONOTONIC, &before);
    ret = ftt_se_wrapper_send(application_stream_ptr->stream_ptr, temp, application_stream_ptr->stream_size + (sizeof (unsigned int)*2), 1);
    clock_gettime(CLOCK_MONOTONIC, &after);

    if (ret == 1) {
        timespec_sub(&difference, &after, &before);
        monitorLogSenderProducer(application_stream_ptr->msg_id, application_stream_ptr->sequence_number, &(application_stream_ptr->monitor_queue), (float) (difference.tv_sec * 1000000000 + difference.tv_nsec) / 1000000);
        lock_access(application_stream_ptr);
        application_stream_ptr->sequence_number++;
        unlock_access(application_stream_ptr);
    }

    if (ret < 0) {
        if (ret == -4) sendError(application_stream_ptr->msg_id, "Message Size is bigger then the Stream Size");
        if (ret == -99) sendError(application_stream_ptr->msg_id, "Stream Closed!");
        return ret;
    }

    free(temp);

    return ret;
}

int receive(application_stream * application_stream_ptr, void* msg_ptr, unsigned int *received_msg_size) {

    if (application_stream_ptr == 0) {
        return -1;
    }

    void* temp;
    temp = calloc(1, application_stream_ptr->stream_size + (sizeof (unsigned int)*2));

    int ret;
    struct timespec before, after, difference;
    //printf("\n Receiveid %d vs %d", received_msg_size , );

    clock_gettime(CLOCK_MONOTONIC, &before);
    ret = ftt_se_wrapper_receive(application_stream_ptr->stream_ptr, temp, application_stream_ptr->stream_size + (sizeof (unsigned int)*2));
    clock_gettime(CLOCK_MONOTONIC, &after);

    if (ret == 1) {
        timespec_sub(&difference, &after, &before);
        /*if ((*(unsigned int*) temp - 1) != application_stream_ptr->last_sequence_number) {
            sendError("Sequence Numbers Inconsistency");
        }*/
        application_stream_ptr->last_sequence_number = *(unsigned int*) temp;
        monitorLogSenderConsumer(application_stream_ptr->msg_id, *(unsigned int*) temp, &(application_stream_ptr->monitor_queue), (float) ((difference.tv_sec * 1000000000 + difference.tv_nsec) / 1000000), *(unsigned int*) (temp + sizeof (unsigned int)));
        *received_msg_size = *(unsigned int*) (temp + sizeof (unsigned int));
        memmove(msg_ptr, temp + sizeof (unsigned int)*2, *received_msg_size);
        free(temp);
        return 1;
    }

    if (ret == -99) {
        sendError(application_stream_ptr->msg_id, "Stream Closed!");
    }

    if (ret == -10) {
        sendError(application_stream_ptr->msg_id, "Stream is inconsistent");
    }

    return ret;

}

int start(char *device_name) {
    int ret = ftt_se_wrapper_start(device_name);
    return ret;
}
