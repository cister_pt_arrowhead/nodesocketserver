/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/*
 * File:   Properties.h
 * Author: root
 *
 * Created on July 7, 2016, 3:27 PM
 */

#ifndef PROPERTIES_H
#define PROPERTIES_H

#ifdef __cplusplus
extern "C" {
#endif

#define MIDDLEWARE_PORT 9999
#define MAX_BUFFER_SIZE 1024

    static char PATH_ID[10] = "-path";

    static char ID_REGISTERING_SERVICE[] = "SERVICE_REGISTERING##";
    static char ID_DELETE_SERVICE[] = "SERVICE_DELETING##";
    static char ID_REQUEST_SERVICE[] = "SERVICE_ORCHESTRATION##";
    static char ID_MONITORING_SERVICE[] = "SERVICE_MONITORING##";
    static char ID_SERVICE_ERROR[] = "SERVICE_ERROR##";

    static char MIDDLEWARE[] = "192.168.1.79";


#ifdef __cplusplus
}
#endif

#endif /* PROPERTIES_H */

