#ifndef SOCKETSERVER_H
#define SOCKETSERVER_H

#include<stdio.h>
#include<string.h>    //strlen
#include<stdlib.h>    //strlen
#include<sys/socket.h>
#include<arpa/inet.h> //inet_addr
#include<unistd.h>    //write
#include<pthread.h> //for threading , link with lpthread

#include "core_private.h"
#include "SocketContentHandler.h"
#include "core_public.h"

typedef struct socket_server_connection_thread_data {
    int socket_fd;
    application * application_ptr;
} socket_server_connection_thread_data;

/*
 * This will handle connection for each client
 * */
void * connection_handler(void *connection_handler_thread_data) {
    //Get the socket descriptor
    printf("\n new connection \n");
    socket_server_connection_thread_data * socket_server_thread_data_ptr = (socket_server_connection_thread_data*) connection_handler_thread_data;

    int read_size;
    char *message, client_message[BUFFER_SIZE];

    //Receive a message from client
    while ((read_size = recv(socket_server_thread_data_ptr->socket_fd, client_message, BUFFER_SIZE, 0)) > 0) {
        //end of string marker
        client_message[read_size] = '\0';

        //Send the message back to client
        write(socket_server_thread_data_ptr->socket_fd, client_message, strlen(client_message));
        int ret = 0;
        struct SlaveParameters params;

        ret = fillStreamParameters(client_message, &params);
        if (ret < 0) {
            printf("\nError when filling parameters");
            return (void*) ret;
        }

        printf("\n--------------SlaveParameters (in createStream )-----------------\n");
        printf("params->isProducer: %d\n", (int) params.isProducer);
        printf("params->id: %d\n", (int) params.id);
        printf("params->mtu: %d\n", (int) params.mtu);
        printf("params->period: %d\n", (int) params.period);
        printf("params->size: %d\n", (int) params.size);
        printf("params->type: %hu\n", params.type);
        printf("---------SlaveParameters -----\n");
        printf("params->qos.delay: %d\n", (int) params.qos.delay);
        printf("params->qos.bandwidth: %d\n", (int) params.qos.bandwidth);
        printf("params->qos.responseTime: %d\n", (int) params.qos.responseTime);



        //todo add new parameters to application_stream structure from SlaveParameters structure 
        //handleSocketContent(client_message); old stuff
        if (ret = (add_new_application_stream(socket_server_thread_data_ptr->application_ptr, (unsigned short) params.id, (unsigned int) params.size, (unsigned short) params.period, (unsigned short) params.mtu, (unsigned char) params.qos.bandwidth, (unsigned char) params.qos.delay, (unsigned char) params.qos.responseTime, params.isProducer, params.type)) < 0) {
            printf("Erro antes do return %d\n", ret);
        }

        //clear the message buffer
        memset(client_message, 0, BUFFER_SIZE);
    }

    if (read_size == 0) {
        puts("Client disconnected");
        fflush(stdout);
    } else if (read_size == -1) {
        perror("recv failed");
    }

    return 0;
}

void * runServer(void * server_thread_data) {
    application * application_ptr = (application*) server_thread_data;
    unsigned int port = get_application_socketServerPort(application_ptr);
    printf("\n PORTA %u", port);
    int socket_desc, client_sock, c;
    struct sockaddr_in server, client;

    //Create socket
    socket_desc = socket(AF_INET, SOCK_STREAM, 0);
    if (socket_desc == -1) {
        printf("Could not create socket");
    }

    //Prepare the sockaddr_in structure
    server.sin_family = AF_INET;
    server.sin_addr.s_addr = INADDR_ANY;
    server.sin_port = htons(port);

    //Bind
    if (bind(socket_desc, (struct sockaddr *) &server, sizeof (server)) < 0) {
        //print the error message
        perror("bind failed. Error");
        //return 1;
    }
    //Listen
    listen(socket_desc, 3);
    c = sizeof (struct sockaddr_in);

    pthread_t thread_id;

    while ((client_sock = accept(socket_desc, (struct sockaddr *) &client, (socklen_t*) & c))) {
        puts("Connection accepted");

        socket_server_connection_thread_data * socket_server_thread_data_ptr;

        socket_server_thread_data_ptr = calloc(1, sizeof (socket_server_connection_thread_data));

        socket_server_thread_data_ptr->application_ptr = application_ptr;
        socket_server_thread_data_ptr->socket_fd = client_sock;

        if (pthread_create(&thread_id, NULL, connection_handler, (void*) socket_server_thread_data_ptr) < 0) {
            perror("could not create thread");
            // return 1;
        }

        //Now join the thread , so that we dont terminate before the thread
        //pthread_join( thread_id , NULL);
        puts("Handler assigned");
    }

    if (client_sock < 0) {
        perror("accept failed");
        //return 1;
    }

    //return 0;
}


#endif /* SOCKETSERVER_H */

