/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   SocketClient.h
 * Author: root
 *
 * Created on July 7, 2016, 2:36 PM
 */

#ifndef SOCKETCLIENT_H
#define SOCKETCLIENT_H

#ifdef __cplusplus
extern "C" {
#endif

#include <stdio.h>
#include <stdlib.h>
#include <netdb.h>
#include <netinet/in.h>
#include <fcntl.h>
#include <semaphore.h>
#include <unistd.h>
#include <string.h>

#include "Properties.h"
#include "core_private.h"

    static int monitoring_sendSocket(unsigned char* sockfd_init, int * sockfd, /*application_stream * application_stream_ptr,*/ char * hostName, int portNumber, char * buffer, int bufferSize) {
        int n;

        if (*sockfd_init /*get_application_stream_monitoring_sockfd_init(application_stream_ptr)*/ == 0) {

            struct sockaddr_in serv_addr;
            struct hostent *server;

            /* Create a socket point */
            //set_application_stream_monitoring_sockfd(application_stream_ptr, socket(AF_INET, SOCK_STREAM, 0));
            *sockfd = socket(AF_INET, SOCK_STREAM, 0);

            if (/*get_application_stream_monitoring_sockfd(application_stream_ptr)*/ *sockfd < 0) {
                printf("ERROR opening socket");
                return -1;
            }

            server = gethostbyname(hostName);

            if (server == NULL) {
                printf("ERROR, no such host\n");
                return -1;
            }

            bzero((char *) &serv_addr, sizeof (serv_addr));
            serv_addr.sin_family = AF_INET;
            bcopy((char *) server->h_addr, (char *) &serv_addr.sin_addr.s_addr, server->h_length);
            serv_addr.sin_port = htons(portNumber);

            printf("CONNECTING TO SERVER\n");

            /* Now connect to the server */
            if (connect(*sockfd/*get_application_stream_monitoring_sockfd(application_stream_ptr)*/, (struct sockaddr*) &serv_addr, sizeof (serv_addr)) < 0) {
                printf("ERROR connecting");
                return -1;
            }
            /*set_application_stream_monitoring_sockfd_init(application_stream_ptr, 1);*/
            *sockfd_init = 1;
        }

        n = write(*sockfd/*get_application_stream_monitoring_sockfd(application_stream_ptr)*/, buffer, bufferSize);

        if (n < 0) {
            perror("ERROR writing to socket");
            return -1;
        }

        return 0;
    }

    static int sendSocket(char * buffer, int bufferSize) {
        int portNumber = MIDDLEWARE_PORT;

        int sockfd, n;
        struct sockaddr_in serv_addr;
        struct hostent *server;

        //printf("SENDING THIS MESSAGE TO SERVER: \n");
        //printf("\n\n %s \n\n", buffer);

        //if it is the first connection
        //if (sockfd == -1) {
        /* Create a socket point */
        sockfd = socket(AF_INET, SOCK_STREAM, 0);

        if (sockfd < 0) {
            perror("ERROR opening socket");
            return -1;
        }

        server = gethostbyname(MIDDLEWARE);

        if (server == NULL) {
            fprintf(stderr, "ERROR, no such host\n");
            return -1;
        }

        bzero((char *) &serv_addr, sizeof (serv_addr));
        serv_addr.sin_family = AF_INET;
        bcopy((char *) server->h_addr, (char *) &serv_addr.sin_addr.s_addr, server->h_length);
        serv_addr.sin_port = htons(portNumber);


        /* Now connect to the server */
        if (connect(sockfd, (struct sockaddr*) &serv_addr, sizeof (serv_addr)) < 0) {
            perror("ERROR connecting");
            return -1;
        }

        n = write(sockfd, buffer, bufferSize);

        if (n < 0) {
            perror("ERROR writing to socket");
            return -1;
        }
        close(sockfd);
        //printf("\nSENT\n");
        return 0;
    }

    static int socket_orchestrateService(char * buffer, int bufferSize) {
        int portNumber = MIDDLEWARE_PORT;

        int sockfd, n;
        struct sockaddr_in serv_addr;
        struct hostent *server;

        /* Create a socket point */
        sockfd = socket(AF_INET, SOCK_STREAM, 0);

        if (sockfd < 0) {
            perror("ERROR opening socket");
            return -1;
        }

        server = gethostbyname(MIDDLEWARE);

        if (server == NULL) {
            fprintf(stderr, "ERROR, no such host\n");
            return -1;
        }

        bzero((char *) &serv_addr, sizeof (serv_addr));
        serv_addr.sin_family = AF_INET;
        bcopy((char *) server->h_addr, (char *) &serv_addr.sin_addr.s_addr, server->h_length);
        serv_addr.sin_port = htons(portNumber);


        /* Now connect to the server */
        if (connect(sockfd, (struct sockaddr*) &serv_addr, sizeof (serv_addr)) < 0) {
            perror("ERROR connecting");
            return -1;
        }

        /* SEND TO ENTRYPOINT THE ORCHESTRATION*/
        printf("\nSending Orchestration\n");
        n = write(sockfd, buffer, bufferSize);
        printf("\nWaiting...s\n");

        /* WAIT FOR THE SERVER RESPONSE */
        char temp[5];
        int read_size = recv(sockfd, temp, sizeof (temp), 0);
        // IF RESPONSE == "200"
        if (atoi(temp) == 200) {
            printf("\nOrchestration Received\n");
            //sem_post(&createStream);
        } else {
            //ELSE
            printf("\nCannot Orchestrate!\n");
        }
        //

        if (n < 0) {
            perror("ERROR writing to socket");
            return -1;
        }
        close(sockfd);

        return 0;
    }

    static int socket_registrateService(char * buffer, int bufferSize) {
        int portNumber = MIDDLEWARE_PORT;

        int sockfd, n;
        struct sockaddr_in serv_addr;
        struct hostent *server;

        /* Create a socket point */
        sockfd = socket(AF_INET, SOCK_STREAM, 0);

        if (sockfd < 0) {
            perror("ERROR opening socket");
            return -1;
        }

        server = gethostbyname(MIDDLEWARE);

        if (server == NULL) {
            fprintf(stderr, "ERROR, no such host\n");
            return -1;
        }

        bzero((char *) &serv_addr, sizeof (serv_addr));
        serv_addr.sin_family = AF_INET;
        bcopy((char *) server->h_addr, (char *) &serv_addr.sin_addr.s_addr, server->h_length);
        serv_addr.sin_port = htons(portNumber);


        /* Now connect to the server */
        if (connect(sockfd, (struct sockaddr*) &serv_addr, sizeof (serv_addr)) < 0) {
            perror("ERROR connecting");
            return -1;
        }

        /* SEND TO ENTRYPOINT THE ORCHESTRATION*/
        n = write(sockfd, buffer, bufferSize);
        printf("\n Waiting For Response...\n");

        /* WAIT FOR THE SERVER RESPONSE */
        char temp[5];
        int read_size = recv(sockfd, temp, sizeof (temp), 0);
        // IF RESPONSE == "200"
        if (atoi(temp) == 200) {
            printf("\nService Registered Sucessfuly!\n");
        } else {
            //ELSE
            printf("\nService Was Not Registered!\n");
        }
        //

        if (n < 0) {
            perror("ERROR writing to socket");
            return -1;
        }
        close(sockfd);

        return 0;
    }
#ifdef __cplusplus
}
#endif

#endif /* SOCKETCLIENT_H */

