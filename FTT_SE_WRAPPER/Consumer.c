/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   Consumer.c
 * Author: root
 *
 * Created on 12 de Agosto de 2016, 0:08
 */

#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <pthread.h>
#include <unistd.h>
#include <sys/types.h>
#include <fcntl.h>
#include <sys/stat.h>
#include <string.h>

#include "core_public.h"

#define myfifo "/tmp/video.asf"
#define ATRIBUI_THREAD_SLEEP_TIME 10000
#define INTERFACE  "consumer"

/**
 * FUNCAO DEDICADA A APENAS UMA STREAM EM QUE ESTA PREPARADA PARA RECEBER CONTEUDO
 * @param stream
 * @return 
 */
void * thread_video_receiver(void * stream) {
    application_stream * message_stream = (application_stream*) stream;

    unsigned char rec[get_msg_size(message_stream)];
    int cont = 0, fd, n = 0, count = 0, mkfifoResponse = 0;
    unsigned int received_msg_size, i = 0;

    //ESCREVE PARA A QUEUE
    fd = open(myfifo, O_WRONLY);
    if (fd < 0) {
        printf("\n\nERRO!");
        return (void*) -1;
    }

    while (1) {
        i = 0;
        int ret = receive(message_stream, &rec, &received_msg_size);
        if (ret == -99) pthread_exit(NULL);
        if (ret > -3 && ret < 0) {
            continue;
        }

        if (ret == -10) {
            printf("application_receive ret: %d\n", ret);
            break;
        }

        //Clean Screen
        printf("\033c");
        //printf("\nReceived ret:%d received_mesize: %u", ret, received_msg_size);
        if (write(fd, rec, received_msg_size) == -1) {
            printf("\nError writing to PIPE");
            printf("\n\nERRO!");
            return (void*) -1;
        }
        //free(rec);
    }
    close(fd);
}

/**
 * FUNCAO QUE INICIA O SLAVE NO FTTSE
 * @return 
 */
void * consumer() {
    int ret;
    application * consumer_application;
    unsigned int portaServidor = 9997;

    ret = plugin_start(&consumer_application, TYPE_CONSUMER, portaServidor, INTERFACE, thread_video_receiver);
    if (ret < 0) {
        printf("erro fttse_arrowhead_plugin_start \n");
        return (void*) ret;
    }

    if (application_orchestration("Oproperties.txt") < 0) {
        printf("erro application_registration");
        return;
    }
    
    char c;
    scanf("%c", &c);
}

/**
 * FUNCAO QUE INICIA O MPLAYER
 * @return 
 */
void * runMPlayer() {
    system("mplayer /tmp/video.asf -cache 8192 -cache-min 1 -fs -noextbased > /dev/null 2>&1");
}

/**
 * FUNCAO PRINCIPAL DO CONSUMIDOR
 * @return 
 */
void * consumer_execute() {
    printf("Tou no consumidor\n");

    int cont = 0, fd, n = 0, count = 0, mkfifoResponse = 0;
    setlinebuf(stdout);
    unlink(myfifo);

    pthread_t id1;
    if (pthread_create(&id1, NULL, runMPlayer, NULL) < 0) {
        perror("could not create thread");
        return (void*) -1;
    }

    // creating fifo 
    mkfifoResponse = mkfifo(myfifo, 0777);

    if (mkfifoResponse == -1) {
        printf("\nError Creating FIFO");
        // return (EXIT_FAILURE);
    }

    pthread_t id3;
    if (pthread_create(&id3, NULL, consumer, NULL) < 0) {
        printf("consumer.c: Could not create thread");
        return (void*) -1;
    }
    pthread_join(id3, NULL);
    pthread_join(id1, NULL);

}

