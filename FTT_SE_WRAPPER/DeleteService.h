/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   DeleteService.h
 * Author: root
 *
 * Created on July 7, 2016, 4:49 PM
 */

#ifndef DELETESERVICE_H
#define DELETESERVICE_H




#ifdef __cplusplus
extern "C" {
#endif

    /**
     * Deletes Service.
     * @param pathFile
     * @return 
     */
    int deleteService(char * pathFile);

#ifdef __cplusplus
}
#endif

#endif /* DELETESERVICE_H */

