/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
#include <stdio.h>
#include <string.h>

#include "FileReader.h"
#include "Properties.h"
#include "SocketClient.h"

int deleteService(char * pathFile) {

    //char pathFile[1024];
    char buffer[MAX_BUFFER_SIZE];
    char bufferTemp[MAX_BUFFER_SIZE];

    int temp = MAX_BUFFER_SIZE;
    // continue the Arguments Reaging - read filePath
    //getFilePath(argc, argv, pathFile);

    // read file ... and put file content to buffer and set size
    if (openFile(buffer, &temp, pathFile) < 0) {
        //strcpy(outputMesage, "Error Reading The File: ");
        //strcat(outputMesage, pathFile);

        return -1;
    }

    strcpy(bufferTemp, ID_DELETE_SERVICE);
    int size = strlen(ID_DELETE_SERVICE);
    strcat(bufferTemp, "\n");

    // size + 1, 1 from the char \n
    temp += size + 1;
    strcat(bufferTemp, buffer);


    // sendSocket
    if (sendSocket(bufferTemp, temp) < 0) {
        // strcpy(outputMesage, "Error Sending Socket to: ");
        // Address
        //strcat(outputMesage, MIDDLEWARE);

        return -1;
    }
    return 1;
}

