/* 
 * File:   SocketContentHandler.h
 * Author: root
 *
 * Created on 19 de Julho de 2016, 17:51
 */

#ifndef SOCKETCONTENTHANDLER_H
#define SOCKETCONTENTHANDLER_H

#ifdef __cplusplus
extern "C" {
#endif

#include <string.h>
#include <stdio.h>
#include "core_private.h"
#include "core_public.h"

#define BUFFER_SIZE 2024
#define MINIMUM_ARGUMENTS 1

    char MONITOR_RULE[] = "MONITOR##";
    char PRODUCER[] = "PRODUCER##";
    char CONSUMER[] = "CONSUMER##";

    char STREAM_ID[] = "ID";
    char STREAM_SIZE[] = "SIZE";
    char STREAM_PERIOD[] = "PERIOD";
    char STREAM_SYNCHRONOUS[] = "SYNCHRONOUS";

    char QOS_TIMERESPONSE[] = "responseTime";
    char QOS_BANDWIDTH[] = "bandwidth";
    char QOS_DELAY[] = "delay";


    char ARG_INTERFACE[] = "-inte";

    int startFillingStream(char * line, struct SlaveParameters *params) {

        if (strcmp(line, PRODUCER) == 0) {
            params->isProducer = 1;
            return 1;
        } else
            if (strcmp(line, CONSUMER) == 0) {
            params->isProducer = 0;
            return 1;
        }

        // LINE = "OLA MUNDO"
        char * spaceSplit;
        spaceSplit = strtok(line, " ");
        // spaceSplit = "OLA"
        if (spaceSplit == NULL) return 0;
        char * identifier = spaceSplit;

        spaceSplit = strtok(NULL, " ");
        //spaceSplit = "MUNDO"
        if (spaceSplit == NULL) return 0;
        char * value = spaceSplit;


        if (strcmp(identifier, STREAM_ID) == 0) {
            params->id = atoi(value);
            return 2;
        } else
            if (strcmp(identifier, STREAM_PERIOD) == 0) {
            //atoi for unsigned short ?
            params->period = atoi(value);
            return 3;
        } else
            if (strcmp(identifier, STREAM_SIZE) == 0) {
            params->size = atoi(value);
            return 4;
        } else
            if (strcmp(identifier, STREAM_SYNCHRONOUS) == 0) {
            params->type = (unsigned short) atoi(value);
            return 5;
        } else
            if (strcmp(identifier, QOS_BANDWIDTH) == 0) {
            params->qos.bandwidth = 1;
        } else
            if (strcmp(identifier, QOS_DELAY) == 0) {
            params->qos.delay = 1;
        } else
            if (strcmp(identifier, QOS_TIMERESPONSE) == 0) {
            params->qos.responseTime = 1;
        }

        return 0;
    }

    /**
     * This function checks if all parameters are filled.
     * @return 
     */
    int areAllParameterFilled(int sumParameters) {
        int number_of_parameters = 5;
        int areAll = 0;
        while (number_of_parameters > 0) {
            areAll += number_of_parameters;
            number_of_parameters--;
        }

        if (areAll == sumParameters) {
            printf("\n\bAll Parameters Were Succesfully Filled!");
            return 1;
        }

        printf("\n\bOne or more stream Parameters are empty!");
        return -1;
    }

    /**
     * This function reads the content and fills the params accordingly.
     * @param content
     * @param params
     * @return 
     */
    int fillStreamParameters(char * content, struct SlaveParameters *params) {
        char * temp = content;
        int ret = 0, temp1;
        while (temp) {
            char * nextLine = strchr(temp, '\n');
            if (nextLine) *nextLine = '\0'; // temporarily terminate the current line
            temp1 = startFillingStream(temp, params);
            //            printf("\n\bret:%d", temp1);

            ret += temp1;
            //printf("curLine=[%s]\n", temp);
            if (nextLine) *nextLine = '\n'; // then restore newline-char, just to be tidy    
            temp = nextLine ? (nextLine + 1) : NULL;
        }
        return areAllParameterFilled(ret);
    }

    void handleSocketContent(char * content) {
        int ret = 0;
        struct SlaveParameters params;
        printf("\n\n 1");

        ret = fillStreamParameters(content, &params);
        if (ret < 0) {
            printf("\nError when filling parameters");
            return;
        }

    }




#ifdef __cplusplus
}
#endif

#endif /* SOCKETCONTENTHANDLER_H */
