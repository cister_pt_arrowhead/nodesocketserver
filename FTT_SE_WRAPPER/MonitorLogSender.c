
#include <string.h>
#include <time.h>
#include <unistd.h>
#include <pthread.h>

#include "Properties.h"
#include "core_private.h"
#include "SocketClient.h"

//30ms
#define TIME_INTERVAL_NANO_SECONDS 300000000
#define LIST_SIZE 20
#define NUMERO_LOGS_POR_LINHA 1
#define SOCKET_CONTENT_SIZE 400

typedef struct monitor_thread_data {
    struct queue_root ** queue;
    int type;
    unsigned char* sockfd_init;
    int * sockfd;
} monitor_thread_data;

/**
 * Registrates Service
 * @param pathFile
 * @return
 */
int monitorLogSenderProducer(unsigned short stream_msg_id, unsigned short stream_sequence_number, struct queue_root ** queue, float sendTime) {
    char * buffer = malloc(SOCKET_CONTENT_SIZE * sizeof (char));
    //        lock_access(application_stream_ptr);
    sprintf(buffer, "{\"map\":{\"si\":\"%d\",\"sn\":\"%d\",\"stc\":\"true\",\"st\":\"%f\"}}",
            stream_msg_id, stream_sequence_number, sendTime);

    queue_add(*queue, buffer);
    //        unlock_access(application_stream_ptr);

    return 1;
}

/**
 * Registrates Service
 * @param pathFile
 * @return
 */
int monitorLogSenderConsumer(unsigned short stream_msg_id, unsigned int sequence_number, struct queue_root ** queue, float receiveTime, unsigned int contentSize) {
    char * buffer = malloc(SOCKET_CONTENT_SIZE * sizeof (char));
    //lock_access(application_stream_ptr);
    sprintf(buffer, "{\"map\":{\"si\":\"%d\",\"sn\":\"%d\",\"rt\":\"%f\",\"cs\":\"%d\"}}",
            stream_msg_id/*get_application_stream_msg_id(application_stream_ptr)*/, sequence_number, receiveTime, contentSize);
    //printf("\n Push: %s\n", buffer);

    queue_add(*queue/*get_application_stream_monitor_queue(application_stream_ptr)*/, buffer);
    //add_log_monitor(application_stream_ptr, buffer);
    //unlock_access(application_stream_ptr);

    return 1;
}

// Enche uma fila de 15 com o conteudo que esta na queue.
//  Ao mesmo tempo em 300ms em 300ms faz um send dessa fila esvaziando-a posteriormente

void join_all_logs(int type, unsigned char* sockfd_init, int * sockfd, char list[LIST_SIZE][SOCKET_CONTENT_SIZE], int numeroDeLogsNaFila) {
    if (list == NULL) {
        return;
    }

    int index = 0, numeroLogsNumaLinha = 0;
    char SOCKET_MESSAGE_LIST[(SOCKET_CONTENT_SIZE * LIST_SIZE) + 200 ];
    if (/*get_application_stream_type(application_stream_ptr)*/ type == 1) {
        sprintf(SOCKET_MESSAGE_LIST, "%s {%s,\"src\":\"p\",\"logs\":[", ID_MONITORING_SERVICE, arrowhead_system);
    } else {
        sprintf(SOCKET_MESSAGE_LIST, "%s {%s,\"src\":\"c\",\"logs\":[", ID_MONITORING_SERVICE, arrowhead_system);
    }
    char headerWithoutID[200];
    if (/*get_application_stream_type(application_stream_ptr)*/type == 1) {
        sprintf(headerWithoutID, "{%s,\"src\":\"p\",\"logs\":[", arrowhead_system);
    } else {
        sprintf(headerWithoutID, "{%s,\"src\":\"c\",\"logs\":[", arrowhead_system);
    }

    while (index <= numeroDeLogsNaFila) {
        if (numeroLogsNumaLinha == 0 && index != 0) {
            strcat(SOCKET_MESSAGE_LIST, headerWithoutID);
        }

        if (numeroLogsNumaLinha != 0) strcat(SOCKET_MESSAGE_LIST, ",");
        strcat(SOCKET_MESSAGE_LIST, list[index]);
        index++;
        numeroLogsNumaLinha++;
        // se for o ultimo log
        if (index > numeroDeLogsNaFila) {
            strcat(SOCKET_MESSAGE_LIST, "]}\0");
            break;
        } else if (index <= numeroDeLogsNaFila && index % NUMERO_LOGS_POR_LINHA == 0) {
            strcat(SOCKET_MESSAGE_LIST, "]}\n");
            numeroLogsNumaLinha = 0;
            continue;
        }
    }
    monitoring_sendSocket(sockfd_init, sockfd, MIDDLEWARE, MIDDLEWARE_PORT, SOCKET_MESSAGE_LIST, strlen(SOCKET_MESSAGE_LIST));
    return;
}

/**
 *  reads from queues - esta vai ser a THREAD de leitura da queue!!!
 *  vai fazer pop da queue e coloca numa lista com 15 posicoes
 *  e envia de 300ms em 300ms
 * */
void * monitor_socket_sender_thread(void * args) {
    monitor_thread_data * args1 = (monitor_thread_data*) args;

    struct queue_root ** queue = args1->queue;
    int type = args1->type;
    unsigned char* sockfd_init = args1->sockfd_init;
    int * sockfd = args1->sockfd;

    char list[LIST_SIZE][SOCKET_CONTENT_SIZE];

    int index_escrita = -1;
    struct timespec before, after, difference;

    clock_gettime(CLOCK_MONOTONIC, &before);
    while (1) {
        // verifica se esta na altura de enviar para o entry point
        clock_gettime(CLOCK_MONOTONIC, &after);
        timespec_sub(&difference, &after, &before);
        if (difference.tv_nsec >= TIME_INTERVAL_NANO_SECONDS) {
            if (index_escrita > -1) {
                //printf("\n index_escrita: %d\n", index_escrita);
                join_all_logs(type, sockfd_init, sockfd, list, index_escrita);
                index_escrita = -1;
                memset(list, 0, sizeof (list));
            }
            clock_gettime(CLOCK_MONOTONIC, &before);
        }

        if (*queue/*get_application_stream_monitor_queue(application_stream_ptr)*/ == 0) {
            printf("\n ERROR!!!! \n");
        }
        char * p = (char*) queue_get(*queue/*get_application_stream_monitor_queue(application_stream_ptr)*/);
        //printf("\n COntent: %s\n", p);
        if (p == NULL) continue;
        if (strlen(p) == 0) continue;
        if ((int) p == -1) pthread_exit(0);
        //printf("\n p: %s\n", p);

        index_escrita++;
        if (index_escrita >= LIST_SIZE) {
            printf("\nERROR!!! index_escrita %d >= LIST_SIZE %d\n", index_escrita, LIST_SIZE);
            //printf("\n index_escrita: %d\n", index_escrita);
            join_all_logs(type, sockfd_init, sockfd, list, index_escrita);
            index_escrita = -1;
            memset(list, 0, sizeof (list));
            continue;
        }

        // pode dar ERRO
        strncpy(list[index_escrita], p, SOCKET_CONTENT_SIZE);
        free(p);
    }

    return 0;
}
