/*****************************************************************************
 * S_ftt-core.layer_embed.h:
 *****************************************************************************
 * Copyright (C) 2006-2012 the FTT-SE team.
 *
 * Author: Ricardo Marau <marau at fe.up.pt>
 *
 * FTT-SE is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  FTT-SE is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with FTT-SE.  If not, see <http://www.gnu.org/licenses/>.
 *****************************************************************************/

#ifndef S_FTT_CORE_LAYER_EMBED_H
#define S_FTT_CORE_LAYER_EMBED_H

#include "S_db.h"
#include "S_NRDB.h"
#include "S_eth_filter.h"
#include "S_eth_filter.h"
#include "S_nodeID.h"
//#warning sacar fora os includes

/** Initializes the Slave core layer adapted to fit in the Master node. \return 0 - OK */
extern signed char S_FTT_CORE_L_init(
		unsigned char *mac, /**< [in] Pointer to a 6 byted mac address of the running interface */
		unsigned long ec_time /**< [in] Duration of the Elementay Cycle [usec] */
		);

/** Closes the Slave core layer adapted to fit in the Master node. \return 0 - OK */
extern signed char S_FTT_CORE_L_close(void);

/** Forces the NodeID, bypassing the initial protocol for the regular Slaves */
extern signed char S_FTT_CORE_L_Set_NodeID(unsigned char id);

/** Decodes the Embedded Slave to directly decode the contents of the SRDB broadcast, bypassing its transmission and delay */
extern void S_master_decode( void *temp_p );
#endif
