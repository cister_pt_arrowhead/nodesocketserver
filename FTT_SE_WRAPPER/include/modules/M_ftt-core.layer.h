/*****************************************************************************
 * M_ftt-core.layer.h:
 *****************************************************************************
 * Copyright (C) 2006-2012 the FTT-SE team.
 *
 * Author: Ricardo Marau <marau at fe.up.pt>
 *
 * FTT-SE is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  FTT-SE is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with FTT-SE.  If not, see <http://www.gnu.org/licenses/>.
 *****************************************************************************/

#ifndef M_FTT_CORE_LAYER_H
#define M_FTT_CORE_LAYER_H

#include "ftt-core.layer_types.h"

/** \defgroup MASTER_FTT_CORE_LAYER_H FTT core layer - Master API 
 This is the interface for the FTT-SE core layer, implementing the Master view of the FTT-SE basic (core) architecture mechanisms.
 \image html ftt-se_CoreMaster.png
 \image latex ftt-se_CoreMaster.eps "FTT-SE Core Master architecture" width=15cm
 
 In the Core Master we have: \n
 SRDB - \ref SRDB \n
 Scheduler - \ref Scheduler \n
 Dispatcher - \ref M_Dispatcher \n
 Eth_filter - \ref M_eth_filter \n
 App_PnP - \ref app_pnp \n
 Master_ch - \ref M_master_ch
 
 */
//@{

/** Initializes the Master ftt-core layer. \return 0 - OK */
extern signed char M_FTT_CORE_L_init(
		unsigned char *mac, /**< [in] Pointer to a 6 byted mac address of the running interface */
		unsigned long ec_time /**< [in] Duration of the Elementay Cycle [usec] */
		);

/** Closes the Master ftt-core layer. \return 0 - OK */
extern signed char M_FTT_CORE_L_close(void);


/** Blocks waiting that a new applications layer is registered in the Slave nodes.
 *
 * This is useful to have an upper-layered thread watching for the conectivity of new nodes. With this it is possible 
 * to be aware of the network configuration.
 * The thread that calls this function is put to sleep and awaked when a node is being registered in the network.
 * \return 0 - OK */
extern signed char M_FTT_CORE_L_get_appPnP( 
		signed short *app_id, /**< [out] Pointer to return the application ID associated to the Slave now plugged in.
							   This identifier serves as a request to register the upper layer (interface-layer) in the 
							   Slave with the one in the Master. Uppon registration the interface layer shall ack a good/bad
							   registration. 
							   If < 0, the node has been disconnected. */
		unsigned char *nodeID /**< [out] Pointer to return the node-given ID, now plugged in. */
		);

/** Replies the remote application with a positive ack on its registration.
 *
 * Uppon an application being plugged, along with the registration ack/notification process it sends the application channel id's
 * that are used to establich a bi-directional communicaiton between the Master and the recently connected Slave.
 * \return 0 - OK */
extern signed char M_FTT_CORE_L_notify_appPnP_OK( 
		unsigned char app_id, /**< [in] The application ID receiving the acknowledgement */
		unsigned char nodeID, /**< [in] The node ID receiving the acknowledgement */
		unsigned short id_tx, /**< [in] The asynchronous Tx channel */
		unsigned short id_rx  /**< [in] The asynchronous Rx channel */
		);

/** As opposed to #M_FTT_CORE_L_notify_appPnP_OK it replies the remote application with a negative ack on its registration.
 *\return 0 - OK */
extern signed char M_FTT_CORE_L_notify_appPnP_KO(
		unsigned char app_id, /**< [in] The application ID receiving the acknowledgement */
		unsigned char nodeID /**< [in] The node ID receiving the acknowledgement */
		);

/** Gets the nodeID given to the Master. 
  * All nodes, including the MAster are given an FTT core node ID. In the case of the Master
  * this is hardcoded and all the Slave IDs come after sequentially. This function returns the hardcoded
  * ID.
  * \return The node ID */
extern signed int M_FTT_CORE_L_get_master_nodeID(void);

/** Gets the hardcoded table.
  *
  * The hard-coded table is describes a set of communication channels that have been internally added by the FTT core module, prior
  * to any registration through this interface. This function is usefull to track and account for the system requirements 
  * that are being used, such as the channel used to broadcast and sync the requirements tables.
  * 
  * This is used to retrieve the resources statically reserved in the ftt-core layer.
  * \return 0 - OK */
extern signed int M_FTT_CORE_L_get_hardcoded_table(
		MESSAGES_TABLE **table /**< [out] Pointer to return the pointer to the Hardcoded table */
		);

/** Updates the requirements of the network. 
  *
  * This function refreshes the FTT core with a new set of communication requirements. This table is provided as
  * a whole and defines ALL communication channels that go in FTT-SE (except for the hard-coded, see #M_FTT_CORE_L_get_hardcoded_table).
  * When updating, the FTT core detects if a channel is being added new, maintained or removed from the system. The FTT core 
  * figures out the necessary changes to the currently running configuration.
  * \return 0 - OK */
extern signed char M_FTT_CORE_L_update_requirements(MESSAGES_TABLE *dyn_table);




/** Parse the real load of a message if submitted. 
  * 
  * This function computes what the load of a message would be.
  * \return The Message load [bits/sec] */
extern double M_FTT_CORE_L_get_message_load(
		MESSAGE_TYPE type, /**< [in] Message type */
		unsigned int size, /**< [in] Message size [bytes] */
		unsigned int period, /**< [in] Message period [EC's] */
		unsigned int MTU /**< [in] Message MTU [bytes] */
		);

/** Parse the real message size. 
 * 
 * This function computes what the real message size of a message would be. 
 * By real I mean, considering the preamble, headers,...
 * \return The Message size [bytes] */
extern unsigned int M_FTT_CORE_L_get_message_bytesize(
		MESSAGE_TYPE type, /**< [in] Message type */
		unsigned int size, /**< [in] Message size [bytes] */
		unsigned int MTU /**< [in] Message MTU [bytes] */
		);

extern double M_FTT_CORE_L_get_message_max_pkt_iit_load( MESSAGE_TYPE type, unsigned int size, unsigned int MTU ); //bps
extern unsigned int M_FTT_CORE_L_get_max_per_pkt_bytesize( MESSAGE_TYPE type, unsigned int size, unsigned int MTU );
extern unsigned int M_FTT_CORE_L_get_corresponding_period_ticks( MESSAGE_TYPE type, unsigned int size, unsigned int MTU, double BW_load /*bps*/ );
extern unsigned int M_FTT_CORE_L_get_corresponding_size_bytes( MESSAGE_TYPE type, unsigned int period, unsigned int MTU, double BW_load /*bps*/ );


/** Returns the EC period, statically registered.  \return The EC period [usec] */
extern unsigned long int M_FTT_CORE_L_get_EC_time( void ); //usec

/** Returns the max load per Synchronous Message window.  \return The SM window load [bits/sec] */
extern double M_FTT_CORE_L_get_SM_slot_load( void );

/** Returns the max load per Asynchronous Message window.  \return The AM window load [bits/sec] */
extern double M_FTT_CORE_L_get_AM_slot_load( void ); /* bps */

/** Returns the Scheduling policy statically defined to handle the Synchronous traffic. \return The Scheduling policy */
extern RET_SCHED_POLICIES M_FTT_CORE_L_ret_sm_sched_policy(void);

/** Returns the Scheduling policy statically defined to handle the Asynchronous traffic. \return The Scheduling policy */
extern RET_SCHED_POLICIES M_FTT_CORE_L_ret_am_sched_policy(void);

//@}

/** (DEBUG) Prints in the current contents in the requirements data base.*/
extern void M_FTT_CORE_L_print_db(void);

#endif
