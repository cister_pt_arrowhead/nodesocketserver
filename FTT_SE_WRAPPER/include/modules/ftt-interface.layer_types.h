/*****************************************************************************
 * ftt-interface.layer_types.h:
 *****************************************************************************
 * Copyright (C) 2006-2012 the FTT-SE team.
 *
 * Author: Ricardo Marau <marau at fe.up.pt>
 *
 * FTT-SE is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  FTT-SE is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with FTT-SE.  If not, see <http://www.gnu.org/licenses/>.
 *****************************************************************************/

#ifndef  FTT_INTERFACE_LAYER_H
#define  FTT_INTERFACE_LAYER_H

//#define FTT_INT_ASYNC_CH_MAX_SIZE 1400

#define M_FTT_MANAGEMENT_L_MAX_ID (0xFFFF)

/** Maximum number of application nodes the ftt-interface layer can handle */
#define M_INT_MAX_APP_NODES (128)

/** Maximum number of internal asynchronous message channels in this layer. 
 *  \details There are two asynch channels per node - the master is excluded as a node
 *           However, the Master needs an ID to broadcast app_reports. */
#define M_INT_MAX_ASYNC_IDS (M_INT_MAX_APP_NODES*2 + 1) 


/** Base ID number for the internal asynchronous messages */
#define M_INT_NODE_ASYNC_ID_BASE (M_FTT_MANAGEMENT_L_MAX_ID + 1 - M_INT_MAX_ASYNC_IDS)

/** The application master ID (hard-coded) */
#define M_INT_APP_MASTER_ID 0

/** Message type */
typedef enum
{
	M_INT_MSG_SYNCH=0,   ///< Synchronous traffic
	M_INT_MSG_ASYNCH_H,  ///< Asynchronous hard real-time traffic
	M_INT_MSG_ASYNCH_S,  ///< Asynchronous Soft real-time traffic
	M_INT_MSG_ASYNCH_U   ///< Asynchronous unconstrained traffic (best-effort)
}M_INT_MSG_TYPE;

/** Defines the ID + Type */
typedef struct {
	unsigned short type;
	unsigned short id;
}__attribute__ ((packed))M_INT_APP_MSG_ID;

#define M_INT_APP_MSG_ID_COMPARE( _id1, _id2 ) ( *((unsigned int *)&_id1) ==  *((unsigned int *)&_id2) )


/** Defines the behavioral of reception call */
typedef enum{
	RX_NO_BLOCK = 0x00,      ///< Non-blocking
	RX_BLOCK_NEW_RX = 0x01,  ///< Block on the reception of a new update at the reception node. To be used with Synchronous traffif. For Asynchronous traffic, it behaves as RX_BLOCK_NEW_TX.
	RX_BLOCK_NEW_TX = 0x02   ///< Block on the reception of new update at the sending node.
}RX_FLAGS;

/** Defines a valid (C,T) entry to be used as a result of a system adaptation. 
 *  \details When either "size" or "period" are negative, means that the continuous range to the previously entered pair is also valid. Example (5,7) (-6,8), indicates a valid result C=[5,6] T={7,8} */
typedef struct{
	signed int size; //if negative indicates a range
	signed int period; //if negative indicates a range
}S_FTT_INTERFACE_L_QOS_PAIR;


#endif

