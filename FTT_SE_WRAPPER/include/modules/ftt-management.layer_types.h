/*****************************************************************************
 * ftt-management.layer_types.h:
 *****************************************************************************
 * Copyright (C) 2006-2012 the FTT-SE team.
 *
 * Author: Ricardo Marau <marau at fe.up.pt>
 *
 * FTT-SE is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  FTT-SE is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with FTT-SE.  If not, see <http://www.gnu.org/licenses/>.
 *****************************************************************************/

#ifndef  M_FTT_MANAGEMENT_LAYER_TYPES_H
#define  M_FTT_MANAGEMENT_LAYER_TYPES_H

/** Defines the ID + Type */
typedef struct {
	unsigned short type;
	unsigned short id;
}M_FTT_MANAGEMENT_L_APP_ID;

#define M_FTT_MANAGEMENT_L_APP_ID_COMPARE( _id1, _id2 ) ( *((unsigned int *)&_id1) ==  *((unsigned int *)&_id2) )

/** (debug) Print the application Message ID */
static inline void M_FTT_MANAGEMENT_L_APP_ID_print(
		M_FTT_MANAGEMENT_L_APP_ID *p,
		unsigned char cr
		)
{
	PRINTF("(type:%d id:0x%x)", p->type, p->id);
	if (cr) PRINTF("\n");
	return ;
}

#define M_FTT_MANAGEMENT_L_MAX_ID (0xFFFF)

/** Message type */
typedef enum{
	M_FTT_MANAGEMENT_NOT_INUSE=0, ///< (internal usage)
	M_FTT_MANAGEMENT_AM,          ///< Asynchronous message
	M_FTT_MANAGEMENT_SM,          ///< Synchronous message
	M_FTT_MANAGEMENT_AM_DUMMY,    ///< (internal usage)
	M_FTT_MANAGEMENT_SM_DUMMY     ///< (internal usage)
}M_FTT_MANAGEMENT_L_MSG_TYPE;

/** The type of transmission in the swiched network */
typedef enum{
	M_FTT_MANAGEMENT_P_UNICAST=0, ///< The message goes to a single destination.
	M_FTT_MANAGEMENT_P_MULTICAST, ///< The message goes to multiple destinations.
	M_FTT_MANAGEMENT_P_BROADCAST  ///< The message goes to all destinations.
}M_FTT_MANAGEMENT_L_MSG_PRODUCTION_TYPE;

/** The sub-type of asynchronous message */
typedef enum{
	M_FTT_MANAGEMENT_HARD_RT=0,   ///< Hard real-time.
	M_FTT_MANAGEMENT_SOFT_RT,     ///< Soft real-time.
	M_FTT_MANAGEMENT_UNCONSTRAINED///< Without real-time requirements... To be handled in best-effort.
}M_FTT_MANAGEMENT_L_MSG_TIMELINESS_TYPE;

/** Defines the Load requirements */
typedef struct{
	unsigned int   m_size;         ///< Message payload size [bytes]
	unsigned int   m_max_size;     /**< Message maximum payload size [bytes]
	                               *   \details Must comprehend any result from an adaptation.*/
	unsigned short m_max_prod_no;  ///< Message maximum allowed number of producers.
	unsigned int   m_period;       ///< Message period [EC's]
	unsigned int   m_deadline;     ///< Message deadline, normally=period 
	signed int     m_init;         ///< EC of first ocurrence. 
	signed int     m_relative_id;  ///< Message id to which m_init is relative; -1 if not applied.
	unsigned int   m_MTU;          ///< Maximum transmission unit [bytes]. \details With implications on the memory fragmentation and the real message size.
	M_FTT_MANAGEMENT_L_MSG_TIMELINESS_TYPE m_timeliness;   ///< Hard/Soft/No constraints - appliable for asynch traffic
}M_FTT_MANAGEMENT_L_LOAD_REQUIREMENTS;

/** Validates the load parameters of a Message. */
static inline unsigned char M_FTT_MANAGEMENT_L_LOAD_REQUIREMENTS_validate(
		M_FTT_MANAGEMENT_L_LOAD_REQUIREMENTS *load_req ///<  [in] Pointer to the load requirements structure.
		)
{

	if ( load_req->m_size > load_req->m_max_size )
		return 0;
	return 1;
}

typedef unsigned short M_FTT_MANAGEMENT_L_CONNECTION;

/** Defines the nodes aggregation on a Message channel, i.e., the Connections of the channel.*/
typedef struct{
	unsigned char Producers_no;  ///< Number of registered Producers (0 or 1).
	unsigned char Consumers_no;  ///< Number of registered Consumers.
	M_FTT_MANAGEMENT_L_CONNECTION Producer_nodeID[255]; ///< Node ID of the producer.
	M_FTT_MANAGEMENT_L_CONNECTION Consumer_nodeID[255]; ///< Array with the Node ID's of the consumers.
	M_FTT_MANAGEMENT_L_MSG_PRODUCTION_TYPE Production_type; ///< How to transmit messages of this channel.
}M_FTT_MANAGEMENT_L_CONNECTION_SET;


#define QOS_MAX_PAIRS_NO 50

/** Defines the QoS bandwidth with the size and period of the channel. */
typedef struct{
	signed int  size;
	signed int  period;
}QOS_BW_PAIR;


/** Defines the QoS requirements */
typedef struct{
	signed short qos_share;
	unsigned char qos_pairs_no; // Number of 
	QOS_BW_PAIR  qos_pairs[QOS_MAX_PAIRS_NO];
	/* For internal use */
	double qos_given_load;  //This represents the excess besides the minimum load
	QOS_BW_PAIR qos_given_pair;
}M_FTT_MANAGEMENT_L_QOS_REQUIREMENTS;

/** Validates the QoS requirements set for the message channel.
 * \details
 *\n  -In the load requirements, the pair (size period) denotes the 
 *\n minimum bandwidth used, i.e. the minimum C and the maximum T.
 *\n  -In the QoS requirements are denoted the ranges in wich the values may varie
 *\n  -The values are stored in an array where the last item represents the
 *\n maximum bandwidth that can be used.
 *\n  -The values must always be stored in decreasing order of T and increasing order of C
 *\n so that the bandwidth used by the pairs grows with the array.
 *\n
 *\n When storing the C or T with a (minus) sign means that all the C or T values 
 *\n in the interval range between the last item and the present are admissible.
 *\n Else, if positive those values that range is not admissible and when changing the value
 *\n a jump must be made.
 *\n if one of the elements is 0 means the the other item admits any value in the range. 
 *\n Example:
 \n*
 *\n (C,T)
 *\n Load->(2,10)
 *\n QoS->(0,8)->(-6,6)   So ([2,6]; {10,8,6})
 *\n QoS->(0,8)->(-6,-6)  So ([2,6]; {10,8}U[8,6])
 *\n QoS->(3,8)->(-6,-6)  So (2;10) + ([3,6]; [8,6])
 *
 * \return 0 - OK
 */
static inline unsigned char M_FTT_MANAGEMENT_L_QOS_REQUIREMENTS_validate( 
		M_FTT_MANAGEMENT_L_QOS_REQUIREMENTS *qos_req,  ///< [in] Pointer to the QoS parameters.
		M_FTT_MANAGEMENT_L_LOAD_REQUIREMENTS *load_req ///< [in] Pointer to the load parameters. 
		)
{
	int i;
	unsigned int Cl,Tl;
	unsigned int Ci,Ti;

	if ( qos_req->qos_pairs_no == 0) return 2;
	if ( qos_req->qos_pairs_no > QOS_MAX_PAIRS_NO) return 0;
	if ( qos_req->qos_share > 100 ) return 0; 

	/* Check if C and T are monotonic */
	Cl = load_req->m_size;
	Tl = load_req->m_period;
	for (i=0; i<(qos_req->qos_pairs_no); i++){
		Ci = ( (qos_req->qos_pairs)[i].size < 0 ) ? -(qos_req->qos_pairs)[i].size : (qos_req->qos_pairs)[i].size;
		Ti = ( (qos_req->qos_pairs)[i].period < 0 ) ? -(qos_req->qos_pairs)[i].period : (qos_req->qos_pairs)[i].period;

		if ( Ci > load_req->m_max_size ) return 0;

		if (Ci){
		       if(Ci<Cl) return 0;
		       Cl=Ci;
		}
		if (Ti){
		       if(Ti>Tl) return 0;
		       Tl=Ti;
		}
	}

//	/* Check if there isn't an ending '0' */
//	if ( (qos_req->qos_pairs)[qos_req->qos_pairs_no].size == 0 ) return 0;
//	if ( (qos_req->qos_pairs)[qos_req->qos_pairs_no].period == 0 ) return 0;
	
	return 1;
}

/** Gets the Bandwidth pair with the highest bandwidth out of the QoS-defined adaption range. \return 0 - OK */
static inline signed char M_FTT_MANAGEMENT_L_QOS_REQUIREMENTS_get_highest_BW_pair(
		M_FTT_MANAGEMENT_L_QOS_REQUIREMENTS *qos_req,   ///< [in] Pointer to the QoS parameters.
		M_FTT_MANAGEMENT_L_LOAD_REQUIREMENTS *load_req, ///< [in] Pointer to the load parameters.
		QOS_BW_PAIR *ret_pair ///< [out] Pointer to return the resulting pair.
		)
{
	QOS_BW_PAIR cur;
	unsigned short i;

	cur.size = load_req->m_size;
	cur.period = load_req->m_period;

	for (i=0; i<qos_req->qos_pairs_no; i++){
		if ( (qos_req->qos_pairs)[i].size != 0) cur.size = (qos_req->qos_pairs)[i].size;
		if ( (qos_req->qos_pairs)[i].period != 0) cur.period = (qos_req->qos_pairs)[i].period;
	}

	if (cur.size < 0) cur.size = -cur.size;
	if (cur.period < 0) cur.period = -cur.period;

	*ret_pair = cur;
	return 0;
}

/** Gets the Bandwidth pair with the highest byte-size out of the QoS-defined adaption range. \return 0 - OK */
static inline signed char M_FTT_MANAGEMENT_L_QOS_REQUIREMENTS_get_highest_size(
		M_FTT_MANAGEMENT_L_QOS_REQUIREMENTS *qos_req,   ///< [in] Pointer to the QoS parameters.
		M_FTT_MANAGEMENT_L_LOAD_REQUIREMENTS *load_req, ///< [in] Pointer to the load parameters.
		unsigned int  *ret_size                         ///< [out] Pointer to return the resulting pair.
		)
{
	signed int cur;
	unsigned short i;

	cur = load_req->m_size;

	for (i=0; i<qos_req->qos_pairs_no; i++){
		if ( (qos_req->qos_pairs)[i].size != 0) cur = (qos_req->qos_pairs)[i].size;
	}

	if (cur<0) cur=-cur;
	*ret_size = cur;
	return 0;
}

/** Gets the Bandwidth pair with the highest period out of the QoS-defined adaption range. \return 0 - OK */
static inline signed char M_FTT_MANAGEMENT_L_QOS_REQUIREMENTS_get_highest_period_until(
		M_FTT_MANAGEMENT_L_QOS_REQUIREMENTS *qos_req,   ///< [in] Pointer to the QoS parameters.
		M_FTT_MANAGEMENT_L_LOAD_REQUIREMENTS *load_req, ///< [in] Pointer to the load parameters.
		unsigned int period_min,                        ///< [in] search until this period.
		unsigned int  *ret_period                       ///< [out] Pointer to return the resulting pair.
		)
{
	signed int cur;
	unsigned int last;
	unsigned short i;
	unsigned char flag;

	/* The periods are set in descending order */

	if (load_req->m_period < period_min){
		*ret_period = load_req->m_period;
		return -1; //exceeded
	}

	last = load_req->m_period;

	for (i=0; i<qos_req->qos_pairs_no; i++){
		cur = (qos_req->qos_pairs)[i].period;
		if ( cur == 0 ) continue;
		if ( cur < 0 ) {cur=-cur; flag=1; }
		else{flag=0;}

		if (cur < period_min){
			/*Well, end of line*/
			if (flag){
				/* Then period_min is on a continous range */
				*ret_period = period_min;
				return 0;
			}else{
				/* Let's check for the history */
				*ret_period = last;
				return 0;
			}
		}
		last = cur;
	}

	/* If I am reaching this point means that I went for all periods 
	 * and all of them are greater than period_min 
	 *  -Let's get the last one */
	*ret_period = last;
	return 0;
}

/** Gets the Bandwidth pair with the highest size until a max size, out of the QoS-defined adaption range. \return 0 - OK */
static inline signed char M_FTT_MANAGEMENT_L_QOS_REQUIREMENTS_get_highest_size_until(
		M_FTT_MANAGEMENT_L_QOS_REQUIREMENTS *qos_req,   ///< [in] Pointer to the QoS parameters.
		M_FTT_MANAGEMENT_L_LOAD_REQUIREMENTS *load_req, ///< [in] Pointer to the load parameters.
		unsigned int size_max,                          ///< [in] The maximum size.
		unsigned int  *ret_size                         ///< [out] Pointer to return the resulting pair.
		)
{
	signed int cur;
	unsigned int last;
	unsigned short i;
	unsigned char flag;

	/* The sizes are set in ascending order */
//PRINT_MSG("In M_FTT_MANAGEMENT_L_QOS_REQUIREMENTS_get_highest_size_until");
//PRINT_MSG("size_max:%d", size_max);
//PRINT_MSG("load_req->m_size:%d", load_req->m_size);

	if (load_req->m_size > size_max){
		*ret_size = load_req->m_size;
		return -1; //exceeded
	}

	last = load_req->m_size;

	for (i=0; i<qos_req->qos_pairs_no; i++){
		cur = (qos_req->qos_pairs)[i].size;
		if ( cur == 0 ) continue;
		if ( cur < 0 ) {cur=-cur; flag=1; }
		else{flag=0;}

		if (cur > size_max){
			/*Well, end of line*/
			if (flag){
				/* Then size_max is on a continous range */
				*ret_size = size_max;
				return 0;
			}else{
				/* Let's check for the history */
				*ret_size = last;
				return 0;
			}
		}
		last = cur;
	}

	/* If I am reaching this point means that I went for all sizes 
	 * and all of them are lesser than size_max 
	 *  -Let's get the last one */
	*ret_size = last;
	return 0;
}

/** Clears the Connection parameters structure. \return 0 - OK */
static inline signed char M_FTT_MANAGEMENT_L_CONNECTION_SET_clear(
		M_FTT_MANAGEMENT_L_CONNECTION_SET *con_req ///< [in] Pointer to the connection parameters.
		)
{
	con_req->Producers_no = 0;
	con_req->Consumers_no = 0;
	con_req->Production_type = M_FTT_MANAGEMENT_P_UNICAST;
	return 0;
}

/** Adds a producer connection to the Connection parameters structure. \return 0 - OK */
static inline signed char M_FTT_MANAGEMENT_L_CONNECTION_SET_merge_producer(
		M_FTT_MANAGEMENT_L_CONNECTION_SET *con_req, ///< [in] Pointer to the connection parameters. 
		unsigned short nodeID ///< Node ID of the producer.
		)
{
	unsigned short i;
	
	/* Check for duplicity */
	for ( i=0; i < con_req->Producers_no; i++ )
		if ( (con_req->Producer_nodeID)[i] == nodeID )
			return 0;
	
	(con_req->Producer_nodeID)[con_req->Producers_no] = nodeID;
	(con_req->Producers_no) ++;
	
	return 0;
}

/** Adds a consumer connection to the Connection parameters structure. \return 0 - OK */
static inline signed char M_FTT_MANAGEMENT_L_CONNECTION_SET_merge_consumer(
		M_FTT_MANAGEMENT_L_CONNECTION_SET *con_req, ///< [in] Pointer to the connection parameters. 
		unsigned short nodeID ///< Node ID of the consumer.
		)
{
	unsigned short i;

	/* Check for duplicity */
	for ( i=0; i < con_req->Consumers_no; i++ )
		if ( (con_req->Consumer_nodeID)[i] == nodeID )
			return 0;
	
	(con_req->Consumer_nodeID)[con_req->Consumers_no] = nodeID;
	(con_req->Consumers_no) ++;
	return 0;
}

/** Validates the parameters set in a connection structure. \return 1 - OK \n 0 - not OK */
static inline signed char M_FTT_MANAGEMENT_L_CONNECTION_SET_validate(
		M_FTT_MANAGEMENT_L_CONNECTION_SET *con_req ///< [in] Pointer to the connection parameters. 
		)
{
	/* A con structure to be valide must have at least:
	 *  - One and only one producer
	 *  - One or several consumers
	 * If any of the above fails then return 0, else return 1.
	 */

	if ( (con_req->Producers_no == 0) || (con_req->Producers_no >= 255) )
		return 0;

	if ((con_req->Consumers_no == 0) && (con_req->Production_type != M_FTT_MANAGEMENT_P_BROADCAST))
		return 0;
	
	return 1;
}


#endif

