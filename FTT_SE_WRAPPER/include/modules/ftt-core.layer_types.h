/*****************************************************************************
 * ftt-core.layer_types.h:
 *****************************************************************************
 * Copyright (C) 2006-2012 the FTT-SE team.
 *
 * Author: Ricardo Marau <marau at fe.up.pt>
 *
 * FTT-SE is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  FTT-SE is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with FTT-SE.  If not, see <http://www.gnu.org/licenses/>.
 *****************************************************************************/

#ifndef  M_FTT_CORE_LAYER_TYPES_H
#define  M_FTT_CORE_LAYER_TYPES_H

#include "Ports.h"


/** The kinds of RT constraints. */
typedef enum
{
	TMLN_UNCONST=0, ///< Unconstrained
	TMLN_HARD, ///< Hard real-time constrained
	TMLN_SOFT ///< Soft real-time constrained
}TIMELINESS_CONSTRAINTS;

/** (debug) Print the timeliness type. */
inline static void TIMELINESS_CONSTRAINTS_print(
		TIMELINESS_CONSTRAINTS *req, /**< [in] Pointer to the Constaints */
		unsigned char f /**< [in] Bool flag to add a line-feed */
		)
{
	switch (*req){
		case TMLN_UNCONST: PRINTF("TMLN_U"); break;
		case TMLN_SOFT: PRINTF("TMLN_S"); break;
		case TMLN_HARD: PRINTF("TMLN_H"); break;
		default: PRINTF("TMLN_?");
	}
	if (f) PRINTF("\n");
}

/** The message requirements. To be used within the requirements table. */
typedef struct{
	unsigned int  m_id;	              ///< Message id
	unsigned int  m_size;                 ///< Size in bytes 
	unsigned int  m_max_size;             /**< \brief Maximum size [bytes].
	                                       *
	                                       *   This must be greater than m_size and denotes the
	                                       *   maximum possible size when this message is modified in the future.
	                                       *   It's value is imutable in the message life-time, with implications
	                                       *   to internal memory-reserved structures.*/
	unsigned int  m_MTU;                  ///< Maximum transmission unit [bytes]. \details With implications on the memory fragmentation and the real message size.
	unsigned int  m_period;               ///< Period in number of ECs.
	unsigned int  m_deadline;             ///< Message deadline [EC's]. 
	signed int    m_init;                 ///< EC of first ocurrence.
	signed int    m_relative_id;          ///< Message id to which m_init is relative; -1 if not applied.
	TIMELINESS_CONSTRAINTS m_timeliness;  ///< For asynchronous messages indicates the sub-class of traffic	timeliness.
	
	unsigned char sw_consumer_idx[255];   ///< List of Slave IDs that consume this message.
	unsigned char sw_consumer_no;	      ///< Number of consumers in the sw_consumer_idx list \details There must be at least one if 0 then it's treated as broadcast.
	unsigned char sw_producer_idx[255];   ///< List of Slave ID producing this message.
	unsigned char sw_producer_no;         ///< Number of producers in the sw_producer_idx list.
	unsigned char sw_producer_max_no;     ///< Defines the maximum allowed number of producers for this var. This is useful to determine the memory size required in the slaves.
}MESSAGE_REQUESITS;

/** (debug) Print the MESSAGE_REQUESITS type. */
inline static void MESSAGE_REQUESITS_print(
		MESSAGE_REQUESITS *req, /**< [in] Pointer to the message requisits */
		unsigned char f /**< [in] Bool flag to add a line-feed */
		)
{
	int i;
	PRINTF("0x%4x Reqs: C%6d T%4d ", req->m_id, req->m_size, req->m_period);
	PRINTF("P.no%d [", req->sw_producer_no);
	for (i=0; i<req->sw_producer_no; i++) PRINTF(" %d", (req->sw_producer_idx)[i]);
	PRINTF("] C.no%d[", req->sw_consumer_no);
	for (i=0; i<req->sw_consumer_no; i++) PRINTF(" %d", (req->sw_consumer_idx)[i]);
	PRINTF("]");
	if ( (req->m_id & 0xE000) == 0x8000 )
		TIMELINESS_CONSTRAINTS_print(&(req->m_timeliness),0);
	if (f) PRINTF("\n");
}

/** Message type */
typedef enum
{
	SYNC_MESSAGE=0,
	ASYNC_MESSAGE=1
}MESSAGE_TYPE;

/** Generic message */
typedef struct{
	MESSAGE_TYPE message_type;  ///< Type of the message being held
	unsigned short message_id;  ///< Message ID
	unsigned char add_type;     ///< Defines how to behave when to add in M_db. \details If to replace as new or modify requirements. Determines overwrite_f flag.
	MESSAGE_REQUESITS req;      ///< The message requirements.
}GEN_MESSAGE_ITEM;

/** Table of message requirements */
typedef struct{
	unsigned short items_no;           ///< Number of items.
	unsigned short items_no_max;       ///< Maximum number of items that it supports.
	GEN_MESSAGE_ITEM *data;            ///< Pointer to the an array of message items.
	rtl_pthread_mutex_t mutex_locker;  ///< Operating mutex locker.
	unsigned char locker_flag;         ///< Locker boolean flag, indicating the locker status.
}MESSAGES_TABLE;

/** Initializes the MESSAGE_TABLE structure. */
inline static signed char MESSAGES_TABLE_Init(
		MESSAGES_TABLE *table, ///< [in] Pointer to the table.
		GEN_MESSAGE_ITEM *data, ///< [in] Pointer to array of GEN_MESSAGE_ITEM.
		unsigned short items_no_max ///< [in] Number of items in the array.
		)
{

	if (table==NULL) return -1;

	if (data==NULL) return -1;

	table->data = data;
	table->items_no_max = items_no_max;
	table->items_no = 0;

	rtl_pthread_mutex_init(&(table->mutex_locker), 0);
	table->locker_flag = 0;

	return 0;
}

/** Closes the MESSAGE_TABLE structure. */
inline static signed char MESSAGES_TABLE_Close(MESSAGES_TABLE *table)
{
	rtl_pthread_mutex_destroy(&(table->mutex_locker));
	table->locker_flag = 0;
	return 0;
}

/** Cleans the MESSAGE_TABLE structure. */
inline static void MESSAGES_TABLE_Clear(MESSAGES_TABLE *table)
{
	table->items_no = 0;
}

/** Enters the mutex lock in the MESSAGE_TABLE structure. 
 * \details The mutex is used to protect the structure in a mutual-exclusion write access. Blocks if the mutex was already locked */
inline static signed char MESSAGES_TABLE_EnterLock(MESSAGES_TABLE *table)
{
	rtl_pthread_mutex_lock(&(table->mutex_locker));
	table->locker_flag = 1;
	return 0;
}

/** Leaves the mutex lock in the MESSAGE_TABLE structure. */
inline static signed char MESSAGES_TABLE_LeaveLock(MESSAGES_TABLE *table)
{
	table->locker_flag = 0;
	rtl_pthread_mutex_unlock(&(table->mutex_locker));
	return 0;
}

/** Checks the mutex lock state in the MESSAGE_TABLE structure. \return 0 - unlocked \n 1 - locked*/
inline static signed char MESSAGES_TABLE_IsLocked(MESSAGES_TABLE *table)
{
	return table->locker_flag;
}

/** Searches for a message ID in the MESSAGE_TABLE structure.
 * \return Pointer to the requirements of the first message found \n NULL - if no match */
inline static  MESSAGE_REQUESITS *MESSAGES_TABLE_Search_ID(
		MESSAGES_TABLE *table, /**< [in] Pointer to the table */
		MESSAGE_TYPE type, /**< [in] Type of message to search */
		unsigned short id /**< [in] ID of Message to search */
		)
{
	unsigned short i;
	for (i=0; i<table->items_no; i++){
		if (table->data[i].message_type == type)
			if (table->data[i].message_id == id){
				return &(table->data[i].req);
			}
	}
	return NULL;
}

/** Appends a new message item in the MESSAGE_TABLE structure. \return 0 - OK */
inline static signed char MESSAGES_TABLE_Append_new(
		MESSAGES_TABLE *table, /**< [in] Pointer to the table */
		MESSAGE_REQUESITS *reqs, /**< [in] Pointer to the message requiremets to add */
		MESSAGE_TYPE type, /**< [in] Type of the adding message */ 
		unsigned short id,  /**< [in] ID of the adding message */
		unsigned char add_type /**< [in] Defines how the massage must be added to the run-time system */
		)
{
	if ( table->items_no == table->items_no_max ){
		ERROR_MSG("Table is full");
		return -1;
	}

	table->data[table->items_no].message_id = id;
	table->data[table->items_no].message_type = type;
	table->data[table->items_no].add_type = add_type;
	table->data[table->items_no].req = *reqs;

	table->items_no++;
	return 0;
}

/** (debug) Print the MESSAGE_TABLE structure. */
inline static void MESSAGES_TABLE_print(
		MESSAGES_TABLE *table, /**< [in] Pointer to the table */
		unsigned char f /**< [in] Bool flag to add a line-feed */
		)
{
	unsigned short i;
	PRINT_MSG("Locker = %d", table->locker_flag );
	PRINT_MSG("%d items", table->items_no);

	for (i=0; i<(table->items_no); i++){
		PRINT_MSG("  item %d:  %s  id:0x%x  add_type: %d",i, ((table->data)[i].message_type==0)?"SYNC":"ASYNC", (table->data)[i].message_id, (table->data)[i].add_type );
	}
}

/** Scheduling policies used to notify the upper layer*/
typedef enum{
	RET_SCHEDD_EDF=0, ///<  EDF
	RET_SCHEDD_RM,    ///< RM
	RET_SCHEDD_DM,    ///< DM
	RET_SCHEDD_FIFO   ///< FIFO
}RET_SCHED_POLICIES;


#ifndef APP_SIGNAL_H
#define APP_SIGNAL_H

#define FTT_SIGNAL_STATUS_OK 1
#define FTT_SIGNAL_STATUS_KO 0

/** Semaphore structure to handle the blocking calls of send and receive within the core layer*/
typedef struct{
	rtl_sem_t semaphore;
	volatile unsigned char status;
}FTT_SIGNAL;

/** Waits for the signal to open.
 * \details The execution is blocked until the semaphore opens
 * \return The semaphore status */
static inline signed char FTT_wait4signal( FTT_SIGNAL *sig )
{
	if (sig->status != FTT_SIGNAL_STATUS_OK){
		ERROR_MSG("The ftt-signal was already destroyed <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<");
		return -1;
	}
//	PRINTF("FTT_wait4signal on %p<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<",&(sig->semaphore));
	rtl_sem_wait(&(sig->semaphore));
	return (sig->status == FTT_SIGNAL_STATUS_KO)? -1 : 0;
}

/** Posts on the semaphore */
static inline void FTT_signal( FTT_SIGNAL *sig )
{
//	PRINTF("FTT_signal on %p<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<",&(sig->semaphore));
	rtl_sem_post(&(sig->semaphore));
}

/** Initializes the semaphore */
static inline signed char FTT_signal_init( FTT_SIGNAL *sig )
{
//	printf("\nsignal init:%p\n",sig);
	sig->status = FTT_SIGNAL_STATUS_OK;
	if ( rtl_sem_init( &(sig->semaphore), 0, 0) < 0){	//Initialization locked
		ERROR_MSG("Couldn't initialize semaphore");
		return -1;
	}
	
	return 0;
}

/** Closes the semaphore structure*/
static inline signed char FTT_signal_destroy( FTT_SIGNAL *sig )
{
	sig->status = FTT_SIGNAL_STATUS_KO;
	rtl_sem_post(&(sig->semaphore)); // wake the 

	sched_yield();

	/* and make sure to clean up the semaphore */
	rtl_sem_destroy(&(sig->semaphore));
	
	return 0;
}

#endif



#endif   /* ----- #ifndef M_FTT-CORE.LAYER_TYPES_FILE_HEADER_INC  ----- */

