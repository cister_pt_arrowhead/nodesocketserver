/*****************************************************************************
 * M_ftt-management.layer.h:
 *****************************************************************************
 * Copyright (C) 2006-2012 the FTT-SE team.
 *
 * Author: Ricardo Marau <marau at fe.up.pt>
 *
 * FTT-SE is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  FTT-SE is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with FTT-SE.  If not, see <http://www.gnu.org/licenses/>.
 *****************************************************************************/

#ifndef  M_FTT_MANAGEMENT_LAYER_H
#define  M_FTT_MANAGEMENT_LAYER_H

#include "ftt-management.layer_types.h"

/** \defgroup MASTER_FTT_MANAGEMENT_LAYER_H FTT management layer - Master API
 This is the interface for the FTT-SE management layer, implementing the FTT-SE management mechanisms, such as distributed connectivity and dynamic QoS management/adaptation.
 \image html ftt-se_ManagementMaster.png
 \image latex ftt-se_ManagementMaster.eps "FTT-SE Management Master architecture" width=15cm
 */
//@{

/** Initializes the Master ftt-management layer. \return 0 - OK */
extern signed char M_FTT_MANAGEMENT_L_init(void);

/** Closes the Master ftt-management layer. \return 0 - OK */
extern signed char M_FTT_MANAGEMENT_L_close(void);

/** Locks the mutual access and prepares the internal databases before submitting changes. \return 0 - OK */
extern signed char M_FTT_MANAGEMENT_L_changes_lock(void);

/** Executes the admission control to verify whether the changes are liable to be submitted. \return 0 - OK*/
extern signed char M_FTT_MANAGEMENT_L_changes_Admission_Control(void);

/** Aborts the changes performed and releases the access. \return 0 - OK*/
extern signed char M_FTT_MANAGEMENT_L_changes_unlock_aborting(void);

/** Submits Aborts the changes performed and unlocks the access. \return 0 - OK*/
extern signed char M_FTT_MANAGEMENT_L_changes_unlock_submiting(void);

/** (debug) Prints the registered requirements */
extern void M_FTT_MANAGEMENT_L_print( unsigned char detailed /** Flag to print detailed information */ );

extern signed int M_FTT_MANAGEMENT_L_get_msg_ftt_id( M_FTT_MANAGEMENT_L_APP_ID app_id );

/** Adds a Message fully described.
 *  \details The characterisation of a Message includes the following set of parameters: Load, QoS and Connection
 *  They can be added individually or together.
 *  \return 0 - OK */
extern signed int M_FTT_MANAGEMENT_L_add_msg_complete( 
		M_FTT_MANAGEMENT_L_APP_ID app_id, ///< [in] The application given ID for the Message channel.
		M_FTT_MANAGEMENT_L_LOAD_REQUIREMENTS *load_req, ///< [in] Pointer to the load requirements.
		M_FTT_MANAGEMENT_L_QOS_REQUIREMENTS *qos_req, ///< [in] Pointer to the qos requirements.
		M_FTT_MANAGEMENT_L_CONNECTION_SET *con_req ///< [in] Pointer to the connection requirements.
		);

/** Deletes a Message from the system \return 0 - OK*/
extern signed char M_FTT_MANAGEMENT_L_del_msg(
		M_FTT_MANAGEMENT_L_APP_ID app_id ///< [in] The application given ID for the Message channel.
		);

/** Assigns the load parameters of a Message \return 0 - OK */
extern signed int M_FTT_MANAGEMENT_L_add_msg_load(
		M_FTT_MANAGEMENT_L_APP_ID app_id, ///< [in] The application given ID for the Message channel.
		M_FTT_MANAGEMENT_L_LOAD_REQUIREMENTS *load_req ///< [in] Pointer to the load parameters.
		);

/** Assigns the QoS parameters of a Message \return 0 - OK */
extern signed int M_FTT_MANAGEMENT_L_add_msg_qos(
		M_FTT_MANAGEMENT_L_APP_ID app_id, ///< [in] The application given ID for the Message channel.
		M_FTT_MANAGEMENT_L_QOS_REQUIREMENTS *qos_req ///< [in] Pointer to the qos parameters.
		);

/** Defines a connection to a Message \return 0 - OK */
extern signed int M_FTT_MANAGEMENT_L_add_msg_connection(
		M_FTT_MANAGEMENT_L_APP_ID app_id, ///< [in] The application given ID for the Message channel.
		M_FTT_MANAGEMENT_L_CONNECTION nodeID, ///< [in] The ID of the node connecting.
		unsigned char direction ///< [in] Defines the connection type. \n 0 - Producer \n 1 - Consumer.
		);

/** Deletes a Message connection \return 0 - OK */
extern signed int M_FTT_MANAGEMENT_L_del_msg_connection(
		M_FTT_MANAGEMENT_L_APP_ID app_id, ///< [in] The application given ID for the Message channel.
		M_FTT_MANAGEMENT_L_CONNECTION nodeID ///< [in] The ID of the node connected.
		);

/** Deletes all connections where a node takes part \return 0 - OK */
extern signed int M_FTT_MANAGEMENT_L_del_node(
		M_FTT_MANAGEMENT_L_CONNECTION nodeID
		);

/** Modifies the period in the load parameters of a Message \return 0 - OK */
extern signed int M_FTT_MANAGEMENT_L_chg_period(
		M_FTT_MANAGEMENT_L_APP_ID app_id, ///< [in] The application given ID for the Message channel.
		unsigned short period ///< [in] New period [EC's].
		);

/** Modifies the QoS fixed priority parameter of a Message \return 0 - OK */
extern signed int M_FTT_MANAGEMENT_L_chg_qos_prio(
		M_FTT_MANAGEMENT_L_APP_ID app_id, ///< [in] The application given ID for the Message channel.
		unsigned short prio ///< [in] New priority.
		);

//@}


#endif

