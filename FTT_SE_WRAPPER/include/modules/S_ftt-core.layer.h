/*****************************************************************************
 * S_ftt-core.layer.h:
 *****************************************************************************
 * Copyright (C) 2006-2012 the FTT-SE team.
 *
 * Author: Ricardo Marau <marau at fe.up.pt>
 *
 * FTT-SE is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  FTT-SE is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with FTT-SE.  If not, see <http://www.gnu.org/licenses/>.
 *****************************************************************************/

#ifndef S_FTT_CORE_LAYER_H
#define S_FTT_CORE_LAYER_H

#include "ftt-core.layer_types.h"

/** \defgroup SLAVE_FTT_CORE_LAYER_H FTT core layer - Slave API 
 This is the interface for the FTT-SE core layer, implementing the Slave view of the FTT-SE basic (core) architecture mechanism.
 To notice that the code in this module is in its majority reused and instantiated in the Master compilation process, with some patches that are enabled/disable with Preprocessing variables.
 
 \image html ftt-se_CoreSlave.png
 \image latex ftt-se_CoreSlave.eps "FTT-SE Core Slave architecture" width=15cm
 
 In the Core Slave we have: \n
 NRDB - \ref NRDB \n
 Dispatcher - \ref S_Dispatcher \n
 Eth_filter - \ref S_eth_filter \n
 App_PnP - \ref S_app_pnp \n
 Master_ch - \ref S_master_ch
 
 */
//@{

/** Initializes the Slave ftt-core layer. \return 0 - OK */
extern signed char S_FTT_CORE_L_init(
		unsigned char *mac /**< [in] Pointer to a 6 byted mac address of the running interface */
		);

/** Closes the Slave ftt-core layer. \return 0 - OK */
extern signed char S_FTT_CORE_L_close(void);

/** Blocks waiting to get identifiers of two asynchronous channels.
 * \details This function is to be called in the initialization process to trigger a configuration request that registers the application (interface layer) in the Master. As a result of this configuration this function returns the channel identifiers that the Master is using to communicate with this Slave in particular. \n This is the function that unblocks the function #M_FTT_CORE_L_get_appPnP in the Master core interface, which triggers the registration procedure in the Master.
 * \return 0 - ok \n -1 - not OK*/
extern signed char S_FTT_CORE_L_Get_asynchronous_buffers(
		unsigned char *app_ID, /**< [in/out] The application ID of the calling thread */
		unsigned short *id_rx, /**< [out] Pointer to return the asynchronous RX channel ID */
		unsigned short *id_tx, /**< [out] Pointer to return the asynchronous TX channel ID */
		unsigned int *ec_len_us/**< [out] Pointer to return the EC lenght */
		);

/** Blocks waiting that a message with a specific FTT ID appears in the Slave database.
 * \details With this function a thread can block waiting that a message ID is registered locally. This basically avoids repeatitify trial 'n error on the messgae binding. It also returns the amount of memory needed in the bind procedure.
 * \return The size of internal memory required to operate with this message var. */
signed int S_FTT_CORE_L_Wait4var(
		MESSAGE_TYPE m_type, /**< [in] Message type */
		unsigned int m_id /**< [in] Message ID */
		);

/** Blocks waiting that a message with a specific FTT ID disappears from the Slave database.
 * \details Contrarily to #S_FTT_CORE_L_Wait4var it waits that a message ID is locally deregistered.*/
signed int S_FTT_CORE_L_Wait4notvar(
		MESSAGE_TYPE m_type, /**< [in] Message type */
		unsigned int m_id /**< [in] Message ID */
		);

/** Binds the application thread to an FTT message variable. 
  * \details After FTT Master has registered the variable and this has been broadcasted and instanciated in the Slaves local databases, the message is ready to be bind. This process allocates memory, creates the internal queues and structures. Only then, the messgae becomes ready to Tx or Rx something. Memory can be allocated in two forms: (1) allocated internally, or (2) provided by the application, as a single memory block that will be internally managed by FTT core. Additionally, this function also returns a callback signal that can be used by the upper layer to monitor Tx/Rx events on this variable. This is particularly useful to implement blocking tx or rx.
  * \return 0 - OK*/
extern signed char S_FTT_CORE_L_bind_mesg(
		MESSAGE_TYPE m_type,      /**< [in] Message type */
		unsigned int m_id,        /**< [in] Message ID */
		FTT_SIGNAL **ret_callback_signal, /**< [out] Pointer to return the address of the FTT_signal structure associated to the binded variable. */
		void *memory_point,       /**< [in] Pointer to a memory pool used to handle the variable buffering. 
		                           *        The memory pool must be given with at least the size returned in S_FTT_CORE_L_Wait4var.
		                           *        If NULL, then the core-layer tries to allocate the required memory from internal resources.*/
		unsigned int mem_size,  /**< [in] mem_size of the given pointer. If memory_point=NULL this is useless */
		unsigned char slots_no    /**< [in] number of slots to be used in this variable. If message type is Synchronous this number should be at least 3. If Asynchronous, slots_no is queue size. */
		);

/** Unbinds the application the FTT message variable. \return 0 - OK*/
extern signed char S_FTT_CORE_L_unbind_mesg(
		MESSAGE_TYPE m_type, /**< [in] Message type */
		unsigned short m_id  /**< [in] Message ID */
		); 

/** Checks the application the FTT message variable. \return 0 - OK*/
extern signed char S_FTT_CORE_L_isbind_mesg(
		MESSAGE_TYPE m_type, /**< [in] Message type */
		unsigned short m_id  /**< [in] Message ID */
		); 

/** Gets the current status of a message variable.
 * \details This function can be used to get the current operational settings after a system adaptation.
 * \return 0 - OK*/
extern signed char S_FTT_CORE_L_get_current(
		MESSAGE_TYPE m_type, /**< [in] Message type */
		unsigned int m_id,  /**< [in] Message ID */
		unsigned char *tag, /**< [out] Pointer to return the current tag */
		unsigned int *cur_size, /**< [out] Pointer to return the current message size [Bytes] */
		unsigned short *period ); /**< [out] Pointer to return the current message period [EC's] */

/** Gets the memory size required to bind a given variable.
 * \details It is useful prior to the bind_mesg call.
 * \return 0 - OK*/
extern signed char S_FTT_CORE_L_get_memory_pool_size(
		MESSAGE_TYPE m_type,
		unsigned int m_id,
		unsigned char slots_no,
		unsigned short *ret_mem_size );

/** Synchronous - Reserves a memory slot to update the synchronous variable status \returns The memory pointer \n NULL - failed */
extern signed char S_FTT_CORE_pre_produce(unsigned short MesgID, unsigned char **return_p);
/** Synchronous - Suspends the Memory reservation procedure to update the synchronous variable status \returns 0 - OK */
extern signed char S_FTT_CORE_pre_produce_abort(unsigned short MesgID);
/** Synchronous - Updates the synchronous variable. \details If there is not a pre reservation, then it updates with the content in buf. The current tag is used as a signature for the content being produced. If the tag is no longer the current one, then the production prcedure fails. \returns 0 - OK */
extern signed char S_FTT_CORE_produce( 
		unsigned short m_id, /**< [in] Message ID */
		unsigned char tag, /**< [in] Message current tag */
		unsigned char *buf /**< [in] Pointer to the data content in case of not having pre-reservation. NULL if there is */
		);

/** Synchronous - Checks whether there is new content to be consumed. \returns 1 - There is new content \n 0 - There is not. */
extern signed char S_FTT_CORE_consume_status( unsigned short MesgID );
/** Synchronous - Reserves the memory slot to consume the available content of a synchronous variable. \returns Pointer to the reserved slot. */
extern unsigned char *S_FTT_CORE_pre_consume(unsigned short MesgID, unsigned char *ret_tag, unsigned char *ret_status );
/** Synchronous - Consumes the content of a synchronous variable. \details If there was pre-reservation, the function frees the reserved slot. Otherwise it copies the content to mesg_value. \returns 0 - OK. */
extern signed char S_FTT_CORE_consume( 
		unsigned short MesgID, /**< [in] Message ID */
		unsigned char *ret_tag, /**< [in] Message current tag */
		unsigned char *mesg_value, /** [out] Pointer where to consume the variable. Only applicable if consuming without pre-reservation. */
		unsigned char *ret_status /** [out] Pointer to return the status flags of the consumed content. */
		);

/** Asynchronous - Reserves a memory slot to send an asynchronous message 
	\returns 0 if OK; 1 if OK but a pre-reservation had been issued; -10 if destroyed ; -7 if FULL */
extern signed char S_FTT_CORE_pre_send(
		unsigned short MesgID,     /**< [in] Message ID */
		unsigned char alloc_block, /**< [in] Determines whether the transmission is blocking (1) or non-blocking (0). */
		unsigned char **return_p   /**  [out]Returns the memory pointer. Only valid if the function returning >=0 */
		);
/** Asynchronous - Suspends the Memory reservation procedure to send the asynchronous message \returns 0 - OK */
extern signed char S_FTT_CORE_pre_send_abort(unsigned short MesgID);
/** Asynchronous - Sends the asynchronous message. \details If there is not a pre reservation, then it sends with the content in buf. The current tag is used as a signature for the content being sent. If the tag is no longer the current one, then the ending prcedure fails. \returns 0 - OK */
extern signed char S_FTT_CORE_send( 
		unsigned short m_id, /**< [in] Message ID */
		unsigned char tag, /**< [in] Message current tag */
		unsigned char *buf, /**< [in] Pointer to the data content in case of not having pre-reservation. NULL if there is */
		unsigned char alloc_block /** [in] Determines whether the transmission is blocking (1) or non-blocking (0). */
		);

/** Asynchronous - Checks whether there is new content to be received. \returns 1 - There is new content \n 0 - There is not. */
extern signed char S_FTT_CORE_receive_status( unsigned short MesgID );
/** Asynchronous - Reserves the memory slot to receive the available content of an asynchronous variable. \returns Pointer to the reserved slot. */
extern unsigned char *S_FTT_CORE_pre_receive(unsigned short MesgID, unsigned char *ret_tag, unsigned char *ret_status);
/** Asynchronous - Receives the content of an asynchronous variable. \details If there was pre-reservation, the function frees the reserved slot. Otherwise it copies the content to mesg_value. \returns 0 - OK. */
extern signed char S_FTT_CORE_receive( 
		unsigned short MesgID, /**< [in] Message ID */
		unsigned char *ret_tag, /**< [in] Message current tag */
		unsigned char *mesg_value, /** [out] Pointer where to receive the variable. Only applicable if receiving without pre-reservation. */
		unsigned char *ret_status /** [out] Pointer to return the status flags of the received content. */
		);

//@}

/** (debug) Print the Slaves current requirements database */
extern void S_FTT_CORE_L_print_db(void);


#endif
