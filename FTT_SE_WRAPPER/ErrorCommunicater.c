/* 
 * File:   ErrorCommunicater.c
 * Author: root
 *
 * Created on 12 de Agosto de 2016, 16:48
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "core_private.h"
#include "Properties.h"
#include "SocketClient.h"


char * arrowhead_system;

void sendError(unsigned short streamID, char * messageError) {
    if (fork() == 0) {
        char buffer[1000];
        sprintf(buffer, "%s{ %s, \"errorMessage\": \"%s\", \"parameters\":{ \"stream_id\": \"%hu\" } }", ID_SERVICE_ERROR, arrowhead_system, messageError, streamID);
        sendSocket(buffer, strlen(buffer) );
        //free(buffer);
    }// SERVICE_ERROR## { "arrowheadSystem": "1" , "errorMessage": messageError }
}
