/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   Producer.c
 * Author: root
 *
 * Created on 12 de Agosto de 2016, 0:08
 */


#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <pthread.h>
#include <unistd.h>
#include <sys/types.h>
#include <fcntl.h>
#include <sys/stat.h>
#include <string.h>

#include "core_public.h"

#define video "video.asf"
#define ATRIBUI_THREAD_SLEEP_TIME 10000
#define VIDEO_SIZE_BY_DEFAULT 2000
#define INTERFACE "producer"

/* 
 * FUNCAO USADA PELO PRODUTOR APENAS 
 */
void * thread_video_sender(void * stream) {
    application_stream * message_stream = (application_stream*) stream;
    int ret = 0;    

    unsigned int video_default_size = get_msg_size(message_stream);
    if (video_default_size <= 0)
        video_default_size = VIDEO_SIZE_BY_DEFAULT;

    // le o ficheiro
    unsigned char buffer[video_default_size];
    int i = 0;
    FILE * source = fopen(video, "rb");
    if (source == NULL) {
        printf("\nVideo File Not Found...");
        return (void*) -1;
    }

    int cont = 0;

    if (!feof(source)) {
        fread(buffer, 1, video_default_size, source);
    } else {
        return (void*) -1;
        printf("\nThe file is empty!");
    }

    while (true) {
        i = 0;
        //printf("\n Sending... %u", video_default_size);
        ret = ssend(stream, &buffer, video_default_size);
        if (ret == -99) pthread_exit(NULL);
        if (ret == -3) {
            printf("Message does not fit\n");
        } else
            if (ret != 1) {
            continue;
        }
        printf("\n Sent!");

        if (!feof(source)) {
            if (fread(buffer, 1, video_default_size, source) <= 0) {
                printf("\nEnd of File! %d:", cont);
                break;
            };
        } else {
            printf("\nEnd of File! %d:", cont);
            cont++;
            break;
        }
        cont++;
        //free(buffer);
    }

}

int producer() {
    int ret;

    application * producer_application;
    unsigned int portaServidor = 9996;
    
    ret = plugin_start(&producer_application, TYPE_PRODUCER, portaServidor, INTERFACE, thread_video_sender);
    if (ret < 0) {
        printf("erro fttse_arrowhead_plugin_start \n");
        return ret;
    }

    if (application_registration("properties.txt") < 0) {
        printf("erro application_registration");
        return -1;
    }
    char c;
    scanf("%c", &c);

    if (application_deletion("properties.txt") < 0) {
        printf("erro application_registration");
    }
}

void * producer_execute() {
    printf("Tou no produtor\n");
    if (producer() < 0) {
        exit(EXIT_FAILURE);
    }
}

