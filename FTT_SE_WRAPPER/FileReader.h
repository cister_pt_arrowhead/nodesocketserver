/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   FileReader.h
 * Author: root
 *
 * Created on July 7, 2016, 2:21 PM
 */

#ifndef FILEREADER_H
#define FILEREADER_H

#ifdef __cplusplus
extern "C" {
#endif

#include <stdio.h>
#include <string.h>
#include <stdlib.h>
    
#include "Properties.h"

    static int getFilePath(int argc, char *argv[], char * output_pathFile) {
        int index = 2;

        while (index < argc) {
            if ((strcmp(argv[index], PATH_ID)) == 0 && ((index + 1) != argc)) {
                index++;
                strcpy(output_pathFile, argv[index]);
                return 1;
            }
            index++;
        }
        return -1;
    }

    static void remove_all_chars(char** str, char c) {
        char *pr = *str, *pw = *str;
        while (*pr) {
            *pw = *pr++;
            pw += (*pw != c);
        }
        *pw = '\0';
    }

    static int openFile(char * buffer, int * bufferSize, char * filePath) {
        FILE * fp;
        fp = fopen(filePath, "r");

        if (fp == NULL) {
            printf("File Not Found...");
            return -1;
        }

        int index = fread(buffer, 1, *bufferSize, fp);

        buffer[index] = 0;
        *bufferSize = (index + 1);
        //remove_all_chars(buffer, '\n');

        fclose(fp);
        return 1;
    }

    // Usar Este Porque nao esta limitado a um tamanho

    static int openFile1(char ** buffer, long * length, char * path_to_file) {
        *buffer = 0;
        FILE * f = fopen(path_to_file, "rb");

        if (f) {
            fseek(f, 0, SEEK_END);
            *length = ftell(f);
            fseek(f, 0, SEEK_SET);
            *buffer = malloc(*length);
            if (*buffer) {
                fread(*buffer, 1, *length, f);
            }
            fclose(f);
        } else {
            return -1;
        }

        if (*buffer == NULL || strlen(*buffer) == 0) {
            return -1;
        }

        return 1;
    }

#ifdef __cplusplus
}
#endif

#endif /* FILEREADER_H */

