/* 
 * File:   MonitorLogSender.h
 * Author: root
 *
 * Created on 29 de Julho de 2016, 10:47
 */

#ifndef MONITORLOGSENDER_H
#define MONITORLOGSENDER_H

#ifdef __cplusplus
extern "C" {
#endif




    typedef struct monitor_thread_data {
        struct queue_root ** queue;
        int type;
        unsigned char* sockfd_init;
        int * sockfd;
    } monitor_thread_data;

    /**
     * Registrates Service
     * @param pathFile
     * @return
     */
    int monitorLogSenderProducer(unsigned short stream_msg_id, unsigned short stream_sequence_number, struct queue_root ** queue, float sendTime);

    /**
     * Registrates Service
     * @param pathFile
     * @return
     */
    int monitorLogSenderConsumer(unsigned short stream_msg_id, unsigned int sequence_number, struct queue_root ** queue, float receiveTime, unsigned int contentSize);

    /**
     *  reads from queues - esta vai ser a THREAD de leitura da queue!!!
     *  vai fazer pop da queue e coloca numa lista com 15 posicoes
     *  e envia de 300ms em 300ms
     * */
    void * monitor_socket_sender_thread(void * args);

#ifdef __cplusplus
}
#endif

#endif /* MONITORLOGSENDER_H */

