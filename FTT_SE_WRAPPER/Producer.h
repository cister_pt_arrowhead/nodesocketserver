/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   Producer.h
 * Author: root
 *
 * Created on 12 de Agosto de 2016, 0:03
 */

#ifndef PRODUCER_H
#define PRODUCER_H

#ifdef __cplusplus
extern "C" {
#endif

    void * producer_execute();

#ifdef __cplusplus
}
#endif

#endif /* PRODUCER_H */

