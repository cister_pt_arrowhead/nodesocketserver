#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <pthread.h>

#include "ftt-slave.h"
#include "real_time_utils.h"
#include "ftt_se_wrapper.h"

struct stream {
    M_INT_APP_MSG_ID app_msg_id;
    unsigned int msg_size;
    unsigned char sending_tag;
    void * msg_pointer;
    unsigned short period_in_ecs;
    unsigned char fifo_size;
    unsigned char first_tx;
    unsigned char type; /* 1-Producer, 2-consumer, 0-unitialized */
    unsigned char set_here;
    struct timespec last_tx_time;
    int sending_state;
    pthread_mutex_t sending_state_mutex;
    pthread_t sending_thread_id;
};
unsigned char ftt_se_wrapper_init_state = 0;
unsigned int number_of_created_streams = 0;

int ftt_se_wrapper_new_stream(stream ** stream_ptr, unsigned short msg_id, unsigned short sync_type, unsigned char fifo_size) {
    if (ftt_se_wrapper_init_state) {
        stream * temp;
        if ((temp = calloc(1, sizeof (stream))) == 0) {
            return -2;
        }

        switch (sync_type) {
            case 0:
                temp->app_msg_id.type = M_INT_MSG_SYNCH;
                printf("\n M_INT_MSG_SYNCH \n");
                break;
            case 1:
                temp->app_msg_id.type = M_INT_MSG_ASYNCH_H;
                printf("\n M_INT_MSG_ASYNCH_H \n");
                break;
            case 2:
                temp->app_msg_id.type = M_INT_MSG_ASYNCH_S;
                printf("\n M_INT_MSG_ASYNCH_S \n");
                break;
            case 3:
                temp->app_msg_id.type = M_INT_MSG_ASYNCH_U;
                printf("\n M_INT_MSG_ASYNCH_U \n");
                break;
            default:
                return -3;
        }
        temp->sending_state = 0;
        if (temp->app_msg_id.type == M_INT_MSG_SYNCH) {
            pthread_mutex_init(&(temp->sending_state_mutex), 0);
            temp->fifo_size = 3;
        } else {
            if (fifo_size == 0) {
                return -4;
            }
            temp->fifo_size = fifo_size;
        }

        temp->app_msg_id.id = msg_id;
        temp->sending_tag = 0;
        temp->msg_pointer = 0;
        temp->first_tx = 1;
        temp->last_tx_time.tv_nsec = 0;
        temp->last_tx_time.tv_sec = 0;
        temp->type = 0;
        temp->set_here = 0;
        number_of_created_streams++;
        *stream_ptr = temp;
        return 1;
    } else {
        return -1;
    }

}

int ftt_se_wrapper_start(char *device_name) {
    if (!ftt_se_wrapper_init_state) {
        if (device_name == 0) {
            return -2;
        }

        /* FTT-SE library slave module initialization*/
        if ((ftt_slave_start_up(0, 0, device_name, 0)) < 0) {

            return -3;
        }
        S_FTT_INTERFACE_L_init_block(0); /* This function blocks until all the FTT-SE initializations are completed*/

        ftt_se_wrapper_init_state = 1;
        return 1;
    } else {

        return -1;
    }

}

int ftt_se_wrapper_stop() {
    if (ftt_se_wrapper_init_state) {
        if (number_of_created_streams != 0) {
            return -2; /* Please close the streams that were opened before terminating or memory leaks will occur */
        }
        ftt_slave_close_up();
        unsigned int ec_size = 0;
        unsigned char ftt_se_wrapper_init_state = 0;
        return 1;
    } else {
        return -1;
    }
}

int ftt_se_wrapper_close_stream(stream * stream_ptr) {
    if (ftt_se_wrapper_init_state) {
        if (stream_ptr == 0) {
            return -2;
        }
        if (stream_ptr->type != 0) {
            if (S_FTT_INTERFACE_L_unbind(stream_ptr->app_msg_id) < 0) {
                return -3;
            }

            if ((S_FTT_INTERFACE_L_dettach(stream_ptr->app_msg_id)) < 0) {
                return -4;
            }
        }
        if (stream_ptr->set_here) {
            if ((S_FTT_INTERFACE_L_var_rm_load(stream_ptr->app_msg_id)) < 0) {
                return -5;
            }
        }
        free(stream_ptr);
        stream_ptr = 0;
        number_of_created_streams--;
        return 1;
    } else {
        return -1;
    }

}

int ftt_se_wrapper_tag_stream(stream * stream_ptr) {
    if (ftt_se_wrapper_init_state) {
        if (stream_ptr == 0) {
            return -2;
        }

        if (stream_ptr->type == 0) {
            return -3; /* Trying to tag an unattached stream */
        }

        unsigned char ret;
        if (((ret = S_FTT_INTERFACE_L_var_details(stream_ptr->app_msg_id, &(stream_ptr->msg_size), &(stream_ptr->period_in_ecs)))) < 0) {
            return -5;
        }
        if (stream_ptr->type == 1) {
            stream_ptr->sending_tag = ret;
        }
        return 1;
    } else {
        return -1;
    }

}

int ftt_se_wrapper_retag_stream(stream * stream_ptr) {
    return ftt_se_wrapper_tag_stream(stream_ptr);
}

int ftt_se_wrapper_bind_stream(stream * stream_ptr) {
    if (ftt_se_wrapper_init_state) {
        if (stream_ptr == 0) {
            return -2;
        }
        if (stream_ptr->type == 0) {
            return -3; /* Trying to bind an unattached stream */
        }

        if ((S_FTT_INTERFACE_L_bind(stream_ptr->app_msg_id, 0, stream_ptr->msg_size, stream_ptr->fifo_size)) < 0) {
            return -4;
        }
        if (ftt_se_wrapper_tag_stream(stream_ptr) < 0) {
            return -5;
        }
        return 1;
    } else {
        return -1;
    }
}

int ftt_se_wrapper_rebind_stream(stream * stream_ptr) {
    return ftt_se_wrapper_bind_stream(stream_ptr);
}

int ftt_se_wrapper_initialize_producer(stream * stream_ptr) {

    if (ftt_se_wrapper_init_state) {
        if (stream_ptr == 0) {
            return -2;
        }

        if (stream_ptr->type > 0) {
            return -3; /* Trying to initialize and already initialized stream*/
        }

        stream_ptr->type = 1;

        if (S_FTT_INTERFACE_L_attach_tx(stream_ptr->app_msg_id) < 0) {
            return -4;
        }


        if (ftt_se_wrapper_bind_stream(stream_ptr) < 0) {
            return -5;
        }

        return 1;
    } else {
        return -1;
    }

}

int ftt_se_wrapper_initialize_consumer(stream * stream_ptr) {
    if (ftt_se_wrapper_init_state) {
        if (stream_ptr == 0) {
            return -2;
        }

        if (stream_ptr->type > 0) {
            return -3; /* Trying to initialize and already initialized stream*/
        }
        stream_ptr->type = 2;

        if ((S_FTT_INTERFACE_L_attach_rx(stream_ptr->app_msg_id)) < 0) {
            return -4;
        }

        if (ftt_se_wrapper_bind_stream(stream_ptr) < 0) {
            return -5;
        }

        return 1;
    } else {
        return -1;
    }
}

int ftt_se_wrapper_set_stream_parameters(stream * stream_ptr, unsigned int msg_size, unsigned short maximum_latency_in_ecs) {
    if (ftt_se_wrapper_init_state) {
        if (stream_ptr == 0) {
            return -2;
        }


        stream_ptr->msg_size = msg_size;

        stream_ptr->period_in_ecs = maximum_latency_in_ecs;

        if ((S_FTT_INTERFACE_L_var_add_load(
                stream_ptr->app_msg_id, /* The application given message ID. */
                stream_ptr->msg_size, /* Message size [bytes]. */
                stream_ptr->msg_size, /* Message maximum size [bytes]. To cope with possible message adaptation. */
                1, /* Message maximum producers no. Implications on the memory size required for the variable. Not fully tested. Use 1 for now! */
                1450, /* Maximum transmission unit [bytes]. */
                stream_ptr->period_in_ecs, /* Message period [EC's] */
                0 /* Not used - Here for API backward compatibility  */)) < 0) {
            return -3;
        } else {
            stream_ptr->set_here = 1;
            return 1;
        }
    } else {
        return -1;
    }
}

int ftt_se_wrapper_send(stream * stream_ptr, void * msg_ptr, unsigned int msg_size, unsigned short blocking_flag) {

    if (ftt_se_wrapper_init_state) {
        if (stream_ptr == 0) {
            return -2;
        }
        if (msg_ptr == 0) {
            return -3;
        }

        if (msg_size > stream_ptr->msg_size) {
            return -4; // Message does not fit stream */
        }

        if (stream_ptr->type == 0) {
            return -5; // Trying to send data on an unitialized stream*/
        }

        if (stream_ptr->type == 2) {
            return -6; //Trying to send data on the consumer side */
        }

        if (stream_ptr->type == M_INT_MSG_SYNCH) {
            blocking_flag = 1;
        }

        int ret2;
        if (((ret2 = S_FTT_INTERFACE_L_pre_send(stream_ptr->app_msg_id, 1 /*queue-block*/, &stream_ptr->msg_pointer))) < 0) {

            if (S_FTT_INTERFACE_L_unbind(stream_ptr->app_msg_id) < 0) {
                return -8;
            }
            return -10;
        }

        if (stream_ptr->msg_pointer == 0) {
            printf("\n Stream Closed!\n");
            return -99;
        } /* Closed Stream */

        //Copying the data to the ftt-se allocated buffer.
        memmove(stream_ptr->msg_pointer, msg_ptr, msg_size);


        int ret;
        if ((ret = S_FTT_INTERFACE_L_send(stream_ptr->app_msg_id, stream_ptr->sending_tag, 0, 1 /*queue-block*/, blocking_flag /*0 non-block 1 block*/)) < 0) {
            switch (ret) {
                case -4:
                    S_FTT_INTERFACE_L_pre_send_abort(stream_ptr->app_msg_id);
                    S_FTT_INTERFACE_L_unbind(stream_ptr->app_msg_id);
                    return -10; //please rebind stream
                case -2:
                    S_FTT_INTERFACE_L_pre_send_abort(stream_ptr->app_msg_id);
                    return -20; //please retag stream
                case -3:
                    return -30; //fifo full please retry until fifo is free
                default:
                    return -40;
                    break;
            }
        }

        return 1;
    } else {

        return -1;
    }

}

typedef struct thread_data {
    stream * stream_ptr;
    void * msg_ptr;
    unsigned int msg_size;
} thread_data;

void destroy_thread_data(thread_data * thread_data_ptr) {
    free(thread_data_ptr->msg_ptr);
    free(thread_data_ptr);
}

void set_sending_state(stream* stream_ptr, int new_value) {
    pthread_mutex_lock(&(stream_ptr->sending_state_mutex));
    stream_ptr->sending_state = new_value;
    pthread_mutex_unlock(&(stream_ptr->sending_state_mutex));
}

int get_sending_state(stream* stream_ptr) {
    pthread_mutex_lock(&(stream_ptr->sending_state_mutex));
    int temp = stream_ptr->sending_state;
    pthread_mutex_unlock(&(stream_ptr->sending_state_mutex));
    return temp;
}

void * ftt_se_wrapper_non_blocking_synchronous_send_thread(void * thread_data_ptr) {
    thread_data * temp_thread_data_ptr = (thread_data *) thread_data_ptr;
    int ret;
    if (((ret = S_FTT_INTERFACE_L_pre_send(temp_thread_data_ptr->stream_ptr->app_msg_id, 1 /*queue-block*/, &(temp_thread_data_ptr->stream_ptr->msg_pointer)))) < 0) {

        if (S_FTT_INTERFACE_L_unbind(temp_thread_data_ptr->stream_ptr->app_msg_id) < 0) {
            set_sending_state(temp_thread_data_ptr->stream_ptr, -1);
            destroy_thread_data(thread_data_ptr);
            pthread_exit(0);
        }
        set_sending_state(temp_thread_data_ptr->stream_ptr, -4);
        destroy_thread_data(thread_data_ptr);
        pthread_exit(0);
    }


    /* Copying the data to the ftt-se allocated buffer.*/
    memmove(temp_thread_data_ptr->stream_ptr->msg_pointer, temp_thread_data_ptr->msg_ptr, temp_thread_data_ptr->msg_size);


    if ((ret = S_FTT_INTERFACE_L_send(temp_thread_data_ptr->stream_ptr->app_msg_id, temp_thread_data_ptr->stream_ptr->sending_tag, 0, 1 /*queue-block*/, 1 /*0 non-block 1 block*/)) < 0) {
        switch (ret) {
            case -4:
                S_FTT_INTERFACE_L_pre_send_abort(temp_thread_data_ptr->stream_ptr->app_msg_id);
                S_FTT_INTERFACE_L_unbind(temp_thread_data_ptr->stream_ptr->app_msg_id);
                set_sending_state(temp_thread_data_ptr->stream_ptr, -1);
                destroy_thread_data(thread_data_ptr);
                pthread_exit(0); //please rebind stream
            case -2:
                S_FTT_INTERFACE_L_pre_send_abort(temp_thread_data_ptr->stream_ptr->app_msg_id);
                set_sending_state(temp_thread_data_ptr->stream_ptr, -2);
                destroy_thread_data(thread_data_ptr);
                pthread_exit(0); //please retag stream
            case -3:
                set_sending_state(temp_thread_data_ptr->stream_ptr, -3);
                destroy_thread_data(thread_data_ptr);
                pthread_exit(0); //fifo full please retry until fifo is free
            default:
                set_sending_state(temp_thread_data_ptr->stream_ptr, -4);
                destroy_thread_data(thread_data_ptr);
                pthread_exit(0);
        }
    }
    set_sending_state(temp_thread_data_ptr->stream_ptr, 0);
    pthread_exit(0);
}

int ftt_se_wrapper_non_blocking_synchronous_send(stream * stream_ptr, void * msg_ptr, unsigned int msg_size) {
    if (ftt_se_wrapper_init_state) {
        if (stream_ptr == 0) {
            return -2;
        }
        if (stream_ptr->app_msg_id.type != M_INT_MSG_SYNCH) {
            return -3; //its not a synchronous message
        }
        if (msg_ptr == 0) {
            return -4;
        }

        if (msg_size > stream_ptr->msg_size) {
            return -5; /* Message does not fit stream */
        }

        if (stream_ptr->type == 0) {
            return -6; /* Trying to send data on an unitialized stream*/
        }

        if (stream_ptr->type == 2) {
            return -7; /* Trying to send data on the consumer side */
        }

        int sending_state = get_sending_state(stream_ptr);

        if (sending_state == -1) {
            return -10;
        }
        if (sending_state == -2) {
            return -20;
        }
        if (sending_state == -3) {
            return -30;
        }
        if (sending_state == -4) {
            return -40;
        }
        if (sending_state == 1) {
            return 2;
        }
        pthread_join(stream_ptr->sending_thread_id, 0);


        thread_data * thread_data_ptr;
        thread_data_ptr = calloc(1, sizeof (thread_data));

        if (thread_data_ptr == 0) {
            return -8;
        }
        thread_data_ptr->stream_ptr = stream_ptr;

        thread_data_ptr->msg_size = msg_size;
        thread_data_ptr->msg_ptr = calloc(1, msg_size);

        memmove(thread_data_ptr->msg_ptr, msg_ptr, msg_size);

        set_sending_state(stream_ptr, 1);
        if (pthread_create(&(stream_ptr->sending_thread_id), NULL, ftt_se_wrapper_non_blocking_synchronous_send_thread, thread_data_ptr)) {
            return -9;
        }

        return 1;
    } else {

        return -1;
    }
}

int ftt_se_wrapper_receive(stream * stream_ptr, void * msg_ptr, unsigned int msg_size) {
    if (ftt_se_wrapper_init_state) {
        int ret;

        if (stream_ptr == 0) {
            return -2;
        }
        if (msg_ptr == 0) {
            return -3;
        }

        if (stream_ptr->type == 0) {
            return -4; /* Trying to recevie data on an unitialized stream*/
        }

        if (stream_ptr->type == 1) {
            return -5; /* Trying to recive data on the producer side */
        }

        if (msg_size > stream_ptr->msg_size) {
            return -6; /**/
        }

        if (S_FTT_INTERFACE_L_pre_receive(stream_ptr->app_msg_id, 0, &stream_ptr->msg_pointer, RX_BLOCK_NEW_TX) < 0) {
            S_FTT_INTERFACE_L_unbind(stream_ptr->app_msg_id);
            return -10;
        }


        if (S_FTT_INTERFACE_L_receive(stream_ptr->app_msg_id, 0, 0, 0) < 0) {
            S_FTT_INTERFACE_L_unbind(stream_ptr->app_msg_id);
            return -10;
        }

        if (stream_ptr->msg_pointer == 0) return -99; /* Closed Stream */
        memmove(msg_ptr, stream_ptr->msg_pointer, msg_size);



        return 1;
    } else {

        return -1;
    }




}

