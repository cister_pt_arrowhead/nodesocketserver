#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "Producer.h"
#include "Consumer.h"


int main(int argc, char**argv) {    
    if (strcmp(argv[1], "consumer") == 0) {
        consumer_execute();
    } else {
        producer_execute();        
    }

    exit(EXIT_SUCCESS);
}