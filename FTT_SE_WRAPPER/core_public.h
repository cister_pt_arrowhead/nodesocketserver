#ifndef CORE_PUBLIC_H
#define CORE_PUBLIC_H

#define TYPE_CONSUMER 2
#define TYPE_PRODUCER 1

struct QoSParameters {
    unsigned char delay;
    unsigned char bandwidth;
    unsigned char responseTime;

};

struct SlaveParameters {
    unsigned char isProducer;
    int id;
    double mtu;
    unsigned short period;
    unsigned int size;
    unsigned short type;
    struct QoSParameters qos;
};


typedef struct application_stream application_stream;

typedef struct application application;

int plugin_start(application ** application_ptr, int type, unsigned int socketServerPort, char * interface, void * functionName);

int application_registration(char * pathToFile);
int application_orchestration(char * pathToFile);
int application_deletion(char * pathToFile);

int ssend(application_stream * application_stream_ptr, void* msg_ptr, unsigned int msg_size);
int receive(application_stream * application_stream_ptr, void* msg_ptr, unsigned int *received_msg_size);


unsigned int get_application_socketServerPort(application * application_ptr);

void set_thread(application_stream * application_stream_ptr, pthread_t sender_thread_id);

pthread_t get_thread(application_stream* application_stream_ptr);

application_stream* get_top_stream(application* application_stream_ptr);
void* get_next_stream(application_stream * application_stream_ptr);
unsigned int get_msg_size(application_stream* application_stream_ptr);
unsigned int get_number_of_streams(application* application);


#endif 
