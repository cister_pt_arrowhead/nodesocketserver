#include <pthread.h>
#include <sched.h>

#include "real_time_utils.h"

struct synchronous_start_param {
    int blocked_threads_counter;
    int number_of_threads;
    pthread_mutex_t synchronous_start_mutex;
    pthread_cond_t thread_condition;
    pthread_cond_t start_condition;
    struct timespec synchronous_activation_time;
};

void sync_init(int number_of_threads, synchronous_start_param * ss_p) {
    ss_p->blocked_threads_counter = 0;
    ss_p->number_of_threads = number_of_threads;
    pthread_mutex_init(&ss_p->synchronous_start_mutex, NULL);
    pthread_cond_init(&ss_p->start_condition, NULL);
    pthread_cond_init(&ss_p->thread_condition, NULL);
}

void sync_wait(struct timespec * current_time, synchronous_start_param * ss_p) {
    pthread_mutex_lock(&ss_p->synchronous_start_mutex);
    ss_p->blocked_threads_counter++;
    if (ss_p->blocked_threads_counter == ss_p->number_of_threads) {
        pthread_cond_signal(&ss_p->start_condition);
    }
    pthread_cond_wait(&ss_p->thread_condition, &ss_p->synchronous_start_mutex);
    current_time->tv_nsec = ss_p->synchronous_activation_time.tv_nsec;
    current_time->tv_sec = ss_p->synchronous_activation_time.tv_sec;
    pthread_mutex_unlock(&ss_p->synchronous_start_mutex);
}

void * synchronous_start_thread(void * param) {
    synchronous_start_param * ss_p = (synchronous_start_param *) param;
    pthread_mutex_lock(&ss_p->synchronous_start_mutex);
    if (ss_p->blocked_threads_counter < ss_p->number_of_threads) {
        pthread_cond_wait(&ss_p->start_condition, &ss_p->synchronous_start_mutex);
    }
    pthread_cond_broadcast(&ss_p->thread_condition);
    clock_gettime(CLOCK_MONOTONIC, &ss_p->synchronous_activation_time);
    pthread_mutex_unlock(&ss_p->synchronous_start_mutex);
}

void sync_start(synchronous_start_param * ss_p) {
    pthread_t sync_thread_id;
    pthread_create(&sync_thread_id, NULL, synchronous_start_thread, ss_p);
}

void * waste_cpu_time(void * iterations) {
    volatile int iter = *(int*) iterations;
    while (iter--) {
    }
    *(int*) iterations = 1; //success
}

void timespec_add_us(struct timespec *t, long us) {
    t->tv_nsec += us * 1000;
    if (t->tv_nsec > 1000000000) {
        t->tv_nsec = t->tv_nsec - 1000000000; // + ms*1000000;
        t->tv_sec += 1;
    }
}

void timespec_add_ms(struct timespec *t, long ms) {
    t->tv_nsec += ms * 1000000;
    if (t->tv_nsec > 1000000000) {
        t->tv_nsec = t->tv_nsec - 1000000000; // + ms*1000000;
        t->tv_sec += 1;
    }
}

int timespec_cmp(struct timespec *a, struct timespec *b) {
    if (a->tv_sec > b->tv_sec) return 1;
    else if (a->tv_sec < b->tv_sec) return -1;
    else if (a->tv_sec == b->tv_sec) {
        if (a->tv_nsec > b->tv_nsec) return 1;
        else if (a->tv_nsec == b->tv_nsec) return 0;
        else return -1;
    }
}

int timespec_sub(struct timespec *d, struct timespec *a, struct timespec *b) {
    d->tv_nsec = a->tv_nsec - b->tv_nsec;
    d->tv_sec = a->tv_sec - b->tv_sec;
    if (a->tv_nsec < b->tv_nsec) {
        d->tv_nsec += 1000000000;
        d->tv_sec -= 1;
    }
    return 1;
}
