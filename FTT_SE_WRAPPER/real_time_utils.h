#ifndef __REAL_TIME_UTILS_H__
#define __REAL_TIME_UTILS_H__

#include <time.h>

typedef struct synchronous_start_param synchronous_start_param;

void sync_init(int number_of_threads, synchronous_start_param * ss_p);

void sync_wait(struct timespec * current_time, synchronous_start_param * ss_p);

void sync_start(synchronous_start_param * ss_p);

void * waste_cpu_time(void * iterations);

void timespec_add_us(struct timespec *t, long us);

void timespec_add_ms(struct timespec *t, long ms);

int timespec_cmp(struct timespec *a, struct timespec *b);

int timespec_sub(struct timespec *d, struct timespec *a, struct timespec *b);

#endif 
