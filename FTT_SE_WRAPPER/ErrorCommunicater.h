/* 
 * File:   ErrorCommunicater.h
 * Author: root
 *
 * Created on 12 de Agosto de 2016, 16:45
 */

#ifndef ERRORCOMMUNICATER_H
#define	ERRORCOMMUNICATER_H

#ifdef	__cplusplus
extern "C" {
#endif
    
#include "Properties.h"
    // 
void sendError(unsigned short streamID, char * messageError);


#ifdef	__cplusplus
}
#endif

#endif	/* ERRORCOMMUNICATER_H */

